package com.meritalberta.Utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.TextView;
import android.widget.Toast;

import com.meritalberta.BuildConfig;
import com.meritalberta.R;
import com.google.android.gms.vision.barcode.Barcode;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class MiscUtility {
    private static int BUFFER_SIZE;

    static {
        BUFFER_SIZE = Barcode.PDF417;
    }

    public static void setRect(Rect r, int l, int t, int w, int h) {
        r.set(l, t, l + w, t + h);
    }

    public static int getSign(int number) {
        return number < 0 ? -1 : 1;
    }

    public static int getSign(double number) {
        return number < 0.0d ? -1 : 1;
    }

    public static float radian2Degree(float rad) {
        return (float) ((((double) rad) * 180.0d) / 3.141592653589793d);
    }

    public static float degree2Radian(float deg) {
        return (float) ((((double) deg) * 3.141592653589793d) / 180.0d);
    }

    public static float radian2Gradian(float rad) {
        return (float) ((((double) rad) * 200.0d) / 3.141592653589793d);
    }

    public static float gradian2Radian(float grad) {
        return (float) ((((double) grad) * 3.141592653589793d) / 200.0d);
    }

    public static float radian2Revolution(float rad) {
        return (float) ((((double) rad) * 0.5d) / 3.141592653589793d);
    }

    public static float revolution2Radian(float rev) {
        return (float) ((((double) rev) * 3.141592653589793d) / 0.5d);
    }

    public static float radian2Slope(float rad) {
        return (float) (Math.tan((double) rad) * 100.0d);
    }

    public static String getDegreeStringFromRad(float rad) {
        float angle = radian2Degree(rad);
        return String.format(Locale.US, "%.1f", new Object[]{Float.valueOf(angle)});
    }

    public static String getGradianStringFromRad(float rad) {
        float angle = radian2Gradian(rad);
        return String.format(Locale.US, "%.1f", new Object[]{Float.valueOf(angle)});
    }

    public static String getRevolutionStringFromRad(float rad) {
        float angle = radian2Revolution(rad);
        return String.format(Locale.US, "%.2f", new Object[]{Float.valueOf(angle)});
    }

    public static String getSlopeStringFromRad(float rad) {
        float angle = radian2Slope(rad);
        return String.format(Locale.US, "%.1f", new Object[]{Float.valueOf(angle)});
    }

    public static boolean isInsideEllipse(int ex, int ey, int w, int h, int px, int py) {
        float a = (float) (w / 2);
        float b = (float) (h / 2);
        double F = Math.sqrt((double) ((a * a) - (b * b)));
        return Math.sqrt(((((double) px) + F) * (((double) px) + F)) + ((double) (py * py))) + Math.sqrt(((F - ((double) px)) * (F - ((double) px))) + ((double) (py * py))) <= ((double) (2.0f * a));
    }

    public static boolean isPointInPolygon(float x, float y, float[] vertices, int vertexNum) {
        boolean c = false;
        int j = vertexNum - 1;
        int i = 0;
        while (i < vertexNum) {
            Object obj;
            if (vertices[(i * 2) + 1] > y) {
                obj = 1;
            } else {
                obj = null;
            }
            if (obj != (vertices[(j * 2) + 1] > y ? 1 : null) && x < (((vertices[j * 2] - vertices[i * 2]) * (y - vertices[(i * 2) + 1])) / (vertices[(j * 2) + 1] - vertices[(i * 2) + 1])) + vertices[i * 2]) {
                if (c) {
                    c = false;
                } else {
                    c = true;
                }
            }
            j = i;
            i++;
        }
        return c;
    }

    public static int signf(float value) {
        return ((int) Math.signum(value)) < 0 ? -1 : 1;
    }

    public static int rectGetMinX(Rect rect) {
        return rect.left;
    }

    public static int rectGetMinY(Rect rect) {
        return rect.top;
    }

    public static int rectGetMidX(Rect rect) {
        return (int) Math.round(((double) (rect.left + rect.right)) / 2.0d);
    }

    public static int rectGetMidY(Rect rect) {
        return (int) Math.round(((double) (rect.top + rect.bottom)) / 2.0d);
    }

    public static int rectGetMaxX(Rect rect) {
        return rect.right;
    }

    public static int rectGetMaxY(Rect rect) {
        return rect.bottom;
    }

    public static int leftToCenterInRect(int w, Rect rect) {
        return rect.left + Math.round((float) ((rect.width() - w) / 2));
    }

    public static int topToCenterInRect(int h, Rect rect) {
        return rect.top + Math.round((float) ((rect.height() - h) / 2));
    }

    public static int leftToCenterInRect(int w, int l, int t, int r, int b) {
        return Math.round((float) (((r - l) - w) / 2)) + l;
    }

    public static int topToCenterInRect(int h, int l, int t, int r, int b) {
        return Math.round((float) (((b - t) - h) / 2)) + t;
    }

    public static double convertValue(double inputValue, double fromRangeMin, double fromRangeMax, double toRangeMin, double toRangeMax) {
        return (((inputValue - fromRangeMin) / (fromRangeMax - fromRangeMin)) * (toRangeMax - toRangeMin)) + toRangeMin;
    }

    public static float validateAngle(float angle) {
        if (angle < 0.0f) {
            angle = (float) (((double) (-angle)) - 1.5707963267948966d);
            if (angle < 0.0f) {
                return (float) (6.283185307179586d - ((double) (-angle)));
            }
            return angle;
        } else if (angle > 0.0f) {
            return (float) (4.71238898038469d - ((double) angle));
        } else {
            return angle;
        }
    }

    public static float reverseAngle(float angle) {
        return (float) (6.283185307179586d - ((double) angle));
    }

    public static float to180(float angle) {
        if (((double) angle) > 3.141592653589793d) {
            return (float) (((double) angle) - 6.283185307179586d);
        }
        return angle;
    }

    public static float rotate360Angle(float angle, float deltaAngle) {
        float rotangle = angle - deltaAngle;
        if (rotangle < 0.0f) {
            rotangle = (float) (((double) rotangle) + 6.283185307179586d);
        }
        return (float) (((double) rotangle) - (((double) (((int) Math.floor(((double) rotangle) / 6.283185307179586d)) * 2)) * 3.141592653589793d));
    }

    public static float flipAngle(float angle) {
        return -angle;
    }

    @SuppressLint({"NewApi"})
    public static void rotateView(View view, float to, float pivotX, float pivotY) {
        RotateAnimation anim = new RotateAnimation(to, to, pivotX, pivotY);
        anim.setInterpolator(new LinearInterpolator());
        anim.setStartTime(0);
        anim.setStartOffset(0);
        anim.setDuration(0);
        anim.setFillEnabled(true);
        anim.setFillBefore(true);
        anim.setFillAfter(true);
        view.startAnimation(anim);
    }

    public static void rotateViewAnimation(View view, float from, float to, float pivotX, float pivotY, long duration, AnimationListener listener) {
        RotateAnimation anim = new RotateAnimation(from, to, pivotX, pivotY);
        anim.setInterpolator(new LinearInterpolator());
        anim.setStartTime(0);
        anim.setStartOffset(0);
        anim.setDuration(duration);
        anim.setFillEnabled(true);
        anim.setFillBefore(true);
        anim.setFillAfter(true);
        anim.setAnimationListener(listener);
        view.startAnimation(anim);
    }

    public static int getId(String resourceName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("No resource ID found for: " + resourceName + " / " + c, e);
        }
    }

    public static void sendEmail(Context context, String to, String subject, String body, String fileToAttach) {
        Intent i = new Intent("android.intent.action.SENDTO");
        i.setType("message/rfc822");
        i.setData(Uri.parse(to));
        i.putExtra("android.intent.extra.SUBJECT", subject);
        i.putExtra("android.intent.extra.TEXT", body);
        if (fileToAttach != null) {
            try {
                copyFile(context, fileToAttach, fileToAttach);
                i.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(context.getExternalFilesDir(null) + "/" + fileToAttach)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        i.addFlags(268435457);
        try {
            context.startActivity(Intent.createChooser(i, "Choose an email client ..."));
        } catch (ActivityNotFoundException e2) {
            Toast.makeText(context, "There are no email clients installed.", 0).show();
        }
    }

    public static void setDialogFontSize(Context context, AlertDialog dialog) {
        float fontSize = context.getResources().getDimension(R.dimen.ALERT_DIALOG_FONT_SIZE);
        TextView textView = (TextView) dialog.findViewById(16908299);
        if (textView != null) {
            textView.setTextSize(1, fontSize);
        }
        textView = (TextView) dialog.findViewById(context.getResources().getIdentifier("alertTitle", "id", "android"));
        if (textView != null) {
            textView.setTextSize(1, fontSize);
        }
        dialog.getButton(-1).setTextSize(1, fontSize);
        dialog.getButton(-2).setTextSize(1, fontSize);
    }

    public static void writeToFile(Context context, String filename, String data, boolean appendMode) {
        OutputStreamWriter outputStreamWriter;
        if (appendMode) {
            try {
                outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filename, AccessibilityNodeInfoCompat.ACTION_PASTE));
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        try {
            outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filename, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToFileWithLimitedSize(Context context, String filename, String data, boolean appendMode, long limitedSize) {
        File file = new File(filename);
        if (file != null && file.length() > (limitedSize * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            file.delete();
        }
        writeToFile(context, filename, data, appendMode);
    }

    public static void writeArrayListToFile(Context context, String filename, ArrayList<String> arrayList) {
        String sData = BuildConfig.FLAVOR;
        for (int i = 0; i < arrayList.size(); i++) {
            sData = sData + ((String) arrayList.get(i)) + "\n";
        }
        writeToFile(context, filename, sData, false);
    }

    public static String readFromFileToString(Context context, String filename) {
        String ret = BuildConfig.FLAVOR;
        try {
            InputStream inputStream = context.openFileInput(filename);
            if (inputStream != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String receiveString = BuildConfig.FLAVOR;
                StringBuilder stringBuilder = new StringBuilder();
                while (true) {
                    receiveString = bufferedReader.readLine();
                    if (receiveString == null) {
                        break;
                    }
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return ret;
    }

    public static ArrayList<String> readFromFileToArrayList(Context context, String filename) {
        ArrayList<String> arrayList = new ArrayList();
        try {
            InputStream inputStream = context.openFileInput(filename);
            if (inputStream != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String str = BuildConfig.FLAVOR;
                while (true) {
                    str = bufferedReader.readLine();
                    if (str == null) {
                        break;
                    }
                    arrayList.add(str);
                }
                inputStream.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return arrayList;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void zip(String[] files, String zipFile) throws IOException {
        Throwable th;
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
        BufferedInputStream origin;
        try {
            byte[] data = new byte[BUFFER_SIZE];
            int i = 0;
            BufferedInputStream origin2 = null;
            while (i < files.length) {
                try {
                    origin = new BufferedInputStream(new FileInputStream(files[i]), BUFFER_SIZE);
                    out.putNextEntry(new ZipEntry(files[i].substring(files[i].lastIndexOf("/") + 1)));
                    while (true) {
                        int count = origin.read(data, 0, BUFFER_SIZE);
                        if (count == -1) {
                            break;
                        }
                        out.write(data, 0, count);
                    }
                    origin.close();
                    i++;
                    origin2 = origin;
                } catch (Throwable th2) {
                    th = th2;
                    origin = origin2;
                }
            }
            out.close();
        } catch (Throwable th3) {
            th = th3;
        }
    }

    public static void unzip(String zipFile, String location) throws IOException {
        ZipInputStream zin = null;
        FileOutputStream fout;
        try {
            File f = new File(location);
            if (!f.isDirectory()) {
                f.mkdirs();
            }
            zin = new ZipInputStream(new FileInputStream(zipFile));
            while (true) {
                ZipEntry ze = zin.getNextEntry();
                if (ze != null) {
                    String path = location + ze.getName();
                    if (ze.isDirectory()) {
                        File unzipFile = new File(path);
                        if (!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        fout = new FileOutputStream(path, false);
                        for (int c = zin.read(); c != -1; c = zin.read()) {
                            fout.write(c);
                        }
                        zin.closeEntry();
                        fout.close();
                    }
                } else {
                    zin.close();
                    return;
                }
            }
        } catch (Exception e) {
            Log.e(BuildConfig.FLAVOR, "Unzip exception", e);
        } catch (Throwable th) {
            zin.close();
        }
    }

    public static void copyFile(Context context, String src, String dst) throws IOException {
        copyFile(new File(context.getFilesDir(), src), new File(context.getExternalFilesDir(null), dst));
    }

    private static void copyFile(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
            if (inChannel != null) {
                inChannel.close();
            }
            if (outChannel != null) {
                outChannel.close();
            }
            if (inStream != null) {
                inStream.close();
            }
            if (outStream != null) {
                outStream.close();
            }
        } catch (Throwable th) {
            if (inChannel != null) {
                inChannel.close();
            }
            if (outChannel != null) {
                outChannel.close();
            }
            if (inStream != null) {
                inStream.close();
            }
            if (outStream != null) {
                outStream.close();
            }
        }
    }

    public static long getAppFirstInstallTime(Context context) {
        try {
            if (VERSION.SDK_INT > 8) {
                return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
            }
            return -1;
        } catch (NameNotFoundException e) {
            return -1;
        }
    }

    public static String convertMilliSecondsToDate(long dateInMilliseconds) {
        return DateFormat.format("MM/dd/yyyy", new Date(dateInMilliseconds)).toString();
    }

    public static long convertDateToMilliSeconds(String date) {
        Date d = null;
        try {
            d = new SimpleDateFormat("MM/dd/yyyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d.getTime();
    }

    public static String getUserCountryViaTelephone(Context context) {
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
            String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) {
                return simCountry.toLowerCase(Locale.US);
            }
            if (tm.getPhoneType() != 2) {
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) {
                    return networkCountry.toLowerCase(Locale.US);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

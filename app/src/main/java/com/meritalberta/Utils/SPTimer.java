package com.meritalberta.Utils;

import android.os.Handler;
import android.util.Log;

public class SPTimer {
    private boolean enabled;
    private Handler handler;
    private int intervalMs;
    private boolean oneTime;
    private Runnable runMethod;
    private Runnable timer_tick;

    public SPTimer(Handler handler, Runnable runMethod, int intervalMs) {
        this.enabled = false;
        this.oneTime = false;
        this.timer_tick = new Runnable() {
            public void run() {
                if (SPTimer.this.enabled) {
                    SPTimer.this.handler.post(SPTimer.this.runMethod);
                    if (SPTimer.this.oneTime) {
                        SPTimer.this.enabled = false;
                    } else {
                        SPTimer.this.handler.postDelayed(SPTimer.this.timer_tick, (long) SPTimer.this.intervalMs);
                    }
                }
            }
        };
        this.handler = handler;
        this.runMethod = runMethod;
        this.intervalMs = intervalMs;
    }

    public SPTimer(Handler handler, Runnable runMethod, int intervalMs, boolean oneTime) {
        this(handler, runMethod, intervalMs);
        this.oneTime = oneTime;
    }

    public void start() {
        if (!this.enabled) {
            if (this.intervalMs < 1) {
                Log.e("timer start", "Invalid interval:" + this.intervalMs);
                return;
            }
            this.enabled = true;
            this.handler.postDelayed(this.timer_tick, (long) this.intervalMs);
        }
    }

    public void stop() {
        if (this.enabled) {
            this.enabled = false;
            this.handler.removeCallbacks(this.runMethod);
            this.handler.removeCallbacks(this.timer_tick);
        }
    }

    public boolean isEnabled() {
        return this.enabled;
    }
}

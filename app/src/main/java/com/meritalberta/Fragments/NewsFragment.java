package com.meritalberta.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meritalberta.Activities.MainActivity;
import com.meritalberta.AppDelegate;
import com.meritalberta.Async.PostAsync;
import com.meritalberta.Models.NewsModel;
import com.meritalberta.Models.PostAysnc_Model;
import com.meritalberta.R;
import com.meritalberta.adapters.NewsAdapter;
import com.meritalberta.constants.ServerRequestConstants;
import com.meritalberta.constants.Tags;
import com.meritalberta.interfaces.OnReciveServerResponse;
import com.meritalberta.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Bharat on 13-Dec-16.
 */

public class NewsFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {
    RecyclerView recyclerview_news;
    private NewsAdapter mAdapter;
    public Handler mHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_NEWS);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_NEWS);
    }

    private void initView(View view) {
        recyclerview_news = (RecyclerView) view.findViewById(R.id.recyclerview_news);
        recyclerview_news.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerview_news.setHasFixedSize(true);
        recyclerview_news.setItemAnimator(new DefaultItemAnimator());
        view.findViewById(R.id.img_c_menu).setOnClickListener(this);
        mHandler.sendEmptyMessage(4);
    }

    public void setNewsAdapter(ArrayList<NewsModel> newsModelArrayList) {
        mAdapter = new NewsAdapter(getActivity(), newsModelArrayList);
        recyclerview_news.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 2:
                        break;
                    case 4:
                        execute_newsList();
                        break;
                }
            }
        };
    }

    private void execute_newsList() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            try {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                //  AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.NEWS_LIST,
                        mPostArrayList, this);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(getActivity(), getString(R.string.time_out));
            return;
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.NEWS_LIST)) {
            parseNewsResponse(result);
        }
    }

    private void parseNewsResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.DataFlow);
                ArrayList<NewsModel> newsModelArrayList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    NewsModel newsModel = new NewsModel();
                    newsModel.image = JSONParser.getString(object, Tags.image);
                    newsModel.title = JSONParser.getString(object, Tags.title);
                    newsModel.content = JSONParser.getString(object, Tags.content);
                    newsModel.link = JSONParser.getString(object, Tags.link);
                    if (AppDelegate.isValidString(newsModel.link))
                        newsModelArrayList.add(newsModel);
                }
                setNewsAdapter(newsModelArrayList);
            } else {
                AppDelegate.showAlert(getActivity(), getResources().getString(R.string.alert), JSONParser.getString(jsonObject, Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Server under maintenance, please try after some time.");
        }
    }
}

package com.meritalberta.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.meritalberta.AppDelegate;
import com.meritalberta.R;

/**
 * Created by Heena on 5/26/2016.
 */
public class SplashActivity extends FragmentActivity {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private ImageView img_splash;
    private double currentLatitude = 0, currentLongitude = 0;
    private Handler mHandler;
    public AlertDialog.Builder alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }

        setContentView(R.layout.splash_activity);
        setHandler();
        checkValidUser();
        initView();
        initFirebase();
        AppDelegate.LogT("test_size_used => " + getString(R.string.test_size_used));
        // checkValidUser();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SplashActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SplashActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    private void initView() {
        img_splash = (ImageView) findViewById(R.id.img_splash);
        img_splash.setVisibility(View.VISIBLE);
    }


    private void checkValidUser() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 1000);
    }

    private void initFirebase() {
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //prefs.clearTempPrefs();
    }

}

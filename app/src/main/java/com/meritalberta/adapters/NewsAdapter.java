package com.meritalberta.adapters;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meritalberta.Activities.WebViewDetailActivity;
import com.meritalberta.AppDelegate;
import com.meritalberta.Models.NewsModel;
import com.meritalberta.R;
import com.meritalberta.constants.Tags;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    public int position;
    private final ArrayList<NewsModel> newsModelArrayList;
    View v;
    Activity context;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NewsModel newsModel = newsModelArrayList.get(position);
        if (AppDelegate.isValidString(newsModel.title))
            holder.txt_c_name.setText(newsModel.title);
        if (AppDelegate.isValidString(newsModel.content))
            holder.txt_c_description.setText(newsModel.content);
        if (AppDelegate.isValidString(newsModel.image)) {
            holder.img_loading1.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading1.getDrawable();
                    frameAnimation.setCallback(holder.img_loading1);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) holder.img_loading1.getDrawable()).start();
                }
            });
            imageLoader.loadImage(newsModel.image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    // holder.img_c_news_image.setImageDrawable(context.getResources().getDrawable(R.drawable.));
                    holder.img_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.img_c_news_image.setImageBitmap(loadedImage);
                    holder.img_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
        }
        holder.img_c_news_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, newsModel.link);
                intent.putExtra(Tags.title, newsModel.title);
                context.startActivity(intent);
//                AppDelegate.openURL(context, newsModel.link);
            }
        });
    }

    public NewsAdapter(Activity context, ArrayList<NewsModel> newsModelArrayList) {
        this.context = context;
        this.newsModelArrayList = newsModelArrayList;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getItemCount() {
        return newsModelArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_name, txt_c_date, txt_c_description;
        ImageView img_c_news_image;
        android.widget.ImageView img_loading1;

        public ViewHolder(View itemView) {
            super(itemView);
            img_loading1 = (android.widget.ImageView) itemView.findViewById(R.id.img_loading1);
            img_c_news_image = (ImageView) itemView.findViewById(R.id.img_c_news_image);
            txt_c_name = (TextView) itemView.findViewById(R.id.txt_c_name);
            txt_c_date = (TextView) itemView.findViewById(R.id.txt_c_date);
            txt_c_description = (TextView) itemView.findViewById(R.id.txt_c_description);
        }
    }
}

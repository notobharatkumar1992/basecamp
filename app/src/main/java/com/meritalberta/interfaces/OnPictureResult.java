package com.meritalberta.interfaces;


import android.net.Uri;

public interface OnPictureResult {
    public void setOnReceivePictureResult(String apiName, Uri picUri);
}

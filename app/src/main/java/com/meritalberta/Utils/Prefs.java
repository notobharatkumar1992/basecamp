package com.meritalberta.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Location;

import com.meritalberta.AppDelegate;
import com.meritalberta.Models.UserDataModel;
import com.meritalberta.constants.Tags;
import com.google.gson.Gson;

public class Prefs {
    /**
     * SEREN_SharedPreferences Class is used to maintain sharedpreferences
     */
    /*
     * SEREN_SharedPreferences Members Declarations
	 */
    private Context mContext;
    private SharedPreferences mSharedPreferences, mSharedPreferencesTemp;
    private Editor mEditor;

    private String str_PrefName = "Funday";
    private String str_PrefName1 = "FundayTemp";

    /**
     * SEREN_SharedPreferences Constructor Implementation
     */
    public Prefs(Context context,
                 OnSharedPreferenceChangeListener mOnSharedPreferenceChangeListener) {
        this.mContext = context;
        if (mContext != null) {
            mSharedPreferences = mContext.getSharedPreferences(
                    str_PrefName, Context.MODE_WORLD_WRITEABLE);
            mSharedPreferencesTemp = mContext.getSharedPreferences(
                    str_PrefName1, Context.MODE_WORLD_WRITEABLE);
            if (mOnSharedPreferenceChangeListener != null) {
                mSharedPreferences
                        .registerOnSharedPreferenceChangeListener(mOnSharedPreferenceChangeListener);
                mSharedPreferencesTemp
                        .registerOnSharedPreferenceChangeListener(mOnSharedPreferenceChangeListener);
            }
        } else {
            AppDelegate.LogE("mContext is null at Prefs");
        }
    }

    public Prefs(Context context) {
        this.mContext = context;
        mSharedPreferences = mContext.getSharedPreferences(
                str_PrefName, Context.MODE_WORLD_WRITEABLE);
        mSharedPreferencesTemp = mContext.getSharedPreferences(
                str_PrefName1, Context.MODE_WORLD_WRITEABLE);
    }

    /**
     * This method is used to store String value in SharedPreferences
     */

    public void putStringValue(String editorkey, String editorvalue) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(editorkey, editorvalue);
        mEditor.commit();
        AppDelegate.LogP("putStringValue => editorkey = " + editorkey + ", editorvalue = " + editorvalue);
    }

    public void putStringValueinTemp(String editorkey, String editorvalue) {
        mEditor = mSharedPreferencesTemp.edit();
        mEditor.putString(editorkey, editorvalue);
        mEditor.commit();
        AppDelegate.LogP("putStringValue => editorkey = " + editorkey + ", editorvalue = " + editorvalue);
    }

    public String getStringValuefromTemp(String editorkey, String defValue) {
        String PrefValue = mSharedPreferencesTemp.getString(editorkey, defValue);
        AppDelegate.LogP("getStringValue => editorkey = " + editorkey + ", editorvalue = " + PrefValue);
        return PrefValue;

    }

    /**
     * This method is used to store int value in SharedPreferences
     */

    public void putIntValue(String editorkey, int editorvalue) {
        mEditor = mSharedPreferences.edit();
        mEditor.putInt(editorkey, editorvalue);
        mEditor.commit();
    }

    /**
     * This method is used to store boolean value in SharedPreferences
     */
    public void putBooleanValue(String editorkey, boolean editorvalue) {
        mEditor = mSharedPreferences.edit();
        mEditor.putBoolean(editorkey, editorvalue);
        mEditor.commit();
    }

    /**
     * This method is used to get String value from SharedPreferences
     *
     * @return String PrefValue
     */
    public String getStringValue(String editorkey, String defValue) {
        String PrefValue = mSharedPreferences.getString(editorkey, defValue);
        AppDelegate.LogP("getStringValue => editorkey = " + editorkey + ", editorvalue = " + PrefValue);
        return PrefValue;

    }

    /**
     * This method is used to get int value from SharedPreferences
     *
     * @return int PrefValue
     */
    public int getIntValue(String editorkey, int defValue) {
        int PrefValue = mSharedPreferences.getInt(editorkey, defValue);
        return PrefValue;
    }

    /**
     * This method is used to get boolean value from SharedPreferences
     *
     * @return boolean PrefValue
     */
    public boolean getBooleanValue(String editorkey, boolean defValue) {
        boolean PrefValue = mSharedPreferences.getBoolean(editorkey, defValue);
        return PrefValue;
    }

    public void putCategoryValue(String editorkey, String editorvalue) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(editorkey, editorvalue);
        mEditor.commit();
    }

    public void putAuthKey(String str_AuthKey) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(Tags.auth_key, str_AuthKey);
        mEditor.commit();
        AppDelegate.LogP("putAuthKey = " + str_AuthKey);
    }

    public String getAuthKey() {
//      String str_AuthKey = mSharedPreferences.getString(Tags.auth_key, "[B@2ea1911d");
        String str_AuthKey = mSharedPreferences.getString(Tags.auth_key, "");
        AppDelegate.LogP("getAuthKey = " + str_AuthKey);
        return str_AuthKey;
    }

    public String getAuthToken() {
        String str_auth_token = mSharedPreferences.getString(Tags.auth_token, "");
        //String str_auth_token = mSharedPreferences.getString(Tags.auth_token, "[B@23f0d27c");
//      String str_auth_token = mSharedPreferences.getString(Tags.auth_token, "[B@27e2b34");
//        if (str_auth_token.length() == 0)
//            str_auth_token = "[B@23f0d27c";
        AppDelegate.LogP("getAuthToken = " + str_auth_token);
        return str_auth_token;
    }

    public void putAuthToken(String str_auth_token) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(Tags.auth_token, str_auth_token);
        mEditor.commit();
        AppDelegate.LogP("putAuthToken = " + str_auth_token);
    }

    public void setRemembered(String user_id) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(Tags.remember, user_id);
        mEditor.commit();
        AppDelegate.LogP("setRemembered = " + user_id);
    }

    public String getPrimaryAddress() {
        String str_PrimaryAddress = mSharedPreferences.getString(Tags.primary_address, "");
        AppDelegate.LogP("getPrimaryAddress = " + str_PrimaryAddress);
        return str_PrimaryAddress;
    }

    public void putPrimaryAddress(String str_PrimaryAddress) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(Tags.primary_address, str_PrimaryAddress);
        mEditor.commit();
        AppDelegate.LogP("putPrimaryAddress = " + str_PrimaryAddress);
    }

    /**
     *
     */
    public String getUserId() {
        String user_id = mSharedPreferences.getString(Tags.user_id, "");
        AppDelegate.LogP("getUserId = " + user_id);
        return user_id;
    }

    public void putUserId(String user_id) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(Tags.user_id, user_id);
        mEditor.commit();
        AppDelegate.LogP("putUserId = " + user_id);
    }

    public UserDataModel getUserdata() {
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(Tags.USER_DATA, "");
        UserDataModel obj = gson.fromJson(json, UserDataModel.class);
        AppDelegate.LogP("getUserdata = " + json);
        return obj;
    }

    public void setUserData(UserDataModel result) {
        Editor edit = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(result);
        edit.putString(Tags.USER_DATA, json);
        edit.commit();
        AppDelegate.LogP("setUserData = " + json);
    }

    //getting user name
    public Location getUserCurrentLocationObject() {
        Location targetLocation = new Location("");//provider name is unecessary
        targetLocation.setLatitude(Double.parseDouble(mSharedPreferences.getString(Tags.LAT, "0.0")));//your coords of course
        targetLocation.setLongitude(Double.parseDouble(mSharedPreferences.getString(Tags.LNG, "0.0")));
        targetLocation.setTime(mSharedPreferences.getLong(Tags.ESTIMATE, 0));
        AppDelegate.LogP("getUserCurrentLocationObject = " + mSharedPreferences.getString(Tags.LAT, "0.0") + "," + mSharedPreferences.getString(Tags.LNG, "0.0"));
        return targetLocation;
    }

    public void setUserCurrentLocationObject(Location location) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(Tags.LAT, location.getLatitude() + "");
        mEditor.putString(Tags.LNG, location.getLatitude() + "");
        mEditor.putLong(Tags.ESTIMATE, location.getTime());
        mEditor.commit();
        AppDelegate.LogP("setUserCurrentLocationObject => " + location.getLatitude() + "," + location.getLongitude() + ", " + location.getTime());
    }

    public void setGCMtokeninTemp(String token) {
        mEditor = mSharedPreferencesTemp.edit();
        mEditor.putString(Tags.GCMtoken, token);
        mEditor.commit();
        AppDelegate.LogP("setGCMtoken = " + token);
    }

    //getting user name
    public String getGCMtokenfromTemp() {
        String token = mSharedPreferencesTemp.getString(Tags.GCMtoken, "");//your coords of course
        AppDelegate.LogP("getGCMtokenfromTemp = " + token);
        return token;
    }

    public void setGCMtoken(String token) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(Tags.GCMtoken, token);
        mEditor.commit();
        AppDelegate.LogP("setGCMtoken = " + token);
    }

    public String getGCMtoken() {
        String token = mSharedPreferences.getString(Tags.GCMtoken, "");//your coords of course
        AppDelegate.LogP("getGCMtoken = " + token);
        return token;
    }

    public void clearTempPrefs() {
        try {
            mEditor = mSharedPreferencesTemp.edit();
            mEditor.clear();
            mEditor.commit();
            AppDelegate.LogP("clearTempPrefs");
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public void clearSharedPreference() {
        try {
            mEditor = mSharedPreferences.edit();
            mEditor.clear();
            mEditor.commit();
            AppDelegate.LogP("clearSharedPreference");
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    /*Temp data saved and cleaned after uses*/
    public UserDataModel getTempUserdata() {
        Gson gson = new Gson();
        String json = mSharedPreferencesTemp.getString(Tags.TEMP_USER_DATA, "");
        UserDataModel obj = gson.fromJson(json, UserDataModel.class);
        AppDelegate.LogP("getTempUserdata = " + json);
        return obj;
    }

    public void setTempUserData(UserDataModel result) {
        Editor edit = mSharedPreferencesTemp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(result);
        edit.putString(Tags.TEMP_USER_DATA, json);
        edit.commit();
        AppDelegate.LogP("setTempUserData = " + json);
    }
}

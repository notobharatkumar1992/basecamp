package com.meritalberta.spirit_level;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.meritalberta.R;
import com.meritalberta.Utils.AccelerometerUtility;
import com.meritalberta.Utils.CircleImageView;
import com.meritalberta.Utils.MiscUtility;
import com.meritalberta.Utils.SPImageButton;
import com.meritalberta.Utils.SettingsActivity;
import com.meritalberta.Utils.SoundUtility;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.location.GeofenceStatusCodes;

import java.util.Locale;

import static com.meritalberta.AppDelegate.mScreenHeight;
import static com.meritalberta.AppDelegate.mScreenWidth;

public class SpiritLevelActivity2 extends AppCompatActivity implements OnClickListener, SensorEventListener {
    private double mAngleOxy;
    private double mAngleOzx;
    private double mAngleOzy;
    private int mBubbleCurPos;
    private ImageView mBubbleImage;
    private int mBubbleSoundPeriod;
    private Handler mBubbleSoundTimerHandler;
    private Runnable mBubbleSoundTimerTask;
    private float mBubbleSoundVolume;
    private double[] mCalibration;
    private double mFilteringFactor;
    private double mGravityX;
    private double mGravityY;
    private double mGravityZ;
    private SPImageButton mLockButton;
    private double mOutputAngle;
    //    private TextView mOutputText;
//    private TextView mOutputUnitText;
    private SensorManager mSensorManager;
    protected RelativeLayout mMainLayout;
    public static final int AD_VIEW_ID = 999;
    private CircleImageView cimg;
    private ImageView mTubeImage;
    carbon.widget.ImageView img_c_back;
    carbon.widget.TextView txt_c_reading, txt_c_reading_degree;

    public SpiritLevelActivity2() {
        this.mLockButton = null;
        this.mBubbleImage = null;
        this.mBubbleCurPos = 0;
        this.mBubbleSoundPeriod = GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE;
        this.mBubbleSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE;
        this.mAngleOxy = 0.0d;
        this.mAngleOzy = 0.0d;
        this.mAngleOzx = 0.0d;
        this.mGravityX = 0.0d;
        this.mGravityY = 0.0d;
        this.mGravityZ = 0.0d;
        this.mOutputAngle = 0.0d;
        this.mFilteringFactor = 0.30000001192092896d;
        this.mCalibration = new double[8];
    }


    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mBubbleSoundTimerHandler = new Handler();
        this.mBubbleSoundTimerTask = new Runnable() {
            public void run() {
                boolean isLocked = PreferenceManager.getDefaultSharedPreferences(SpiritLevelActivity2.this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false);
                boolean soundOn = PreferenceManager.getDefaultSharedPreferences(SpiritLevelActivity2.this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true);
                if (!isLocked && soundOn) {
                    SoundUtility.getInstance().playSound(1, SpiritLevelActivity2.this.mBubbleSoundVolume);
                }
                SpiritLevelActivity2.this.mBubbleSoundTimerHandler.postDelayed(this, (long) SpiritLevelActivity2.this.mBubbleSoundPeriod);
            }
        };
        setContentView(R.layout.spirit);
        createUi();
    }
    public static double getDPI(int width, int height, double diagonal) {
        return ((double) width) / (Math.cos(Math.atan2((double) height, (double) width)) * diagonal);
    }

    public static float getScreenDPI(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.xdpi;
    }
    @SuppressLint({"NewApi"})
    private void createUi() {
        super.setRequestedOrientation(0);
        setContentView(R.layout.spirit);
        this.mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        cimg = (CircleImageView) findViewById(R.id.circle);
        ViewTreeObserver viewTreeObserver = cimg.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    SpiritLevelActivity2.this.arrangeLayout();
                    cimg.getLayoutParams().width = cimg.getHeight();
                    cimg.invalidate();
                    cimg.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
        mTubeImage = (ImageView) findViewById(R.id.img_line);
        img_c_back = (carbon.widget.ImageView) findViewById(R.id.img_c_back);
        txt_c_reading = (carbon.widget.TextView) findViewById(R.id.txt_c_reading);
        txt_c_reading_degree = (carbon.widget.TextView) findViewById(R.id.txt_c_reading_degree);
        this.mBubbleImage = new ImageView(this);
        this.mMainLayout.addView(this.mBubbleImage);
        Typeface face = Typeface.createFromAsset(getAssets(), getString(R.string.font_light));
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.txt_c_reading_degree.setText("~");
        } else if (unit.equals("1")) {
            this.txt_c_reading_degree.setText("rad");
        } else if (unit.equals("2")) {
            this.txt_c_reading_degree.setText("gra");
        } else {
            this.txt_c_reading_degree.setText("%");
        }
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false);
        this.mLockButton = new SPImageButton(this);
        this.mLockButton.setId(3);
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(isLocked ? R.drawable.button_lock : R.drawable.button_unlock)).getBitmap());
        this.mLockButton.setOnClickListener(this);
        img_c_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void arrangeLayout() {
        LayoutParams params = new LayoutParams(mScreenWidth, -2);
        params.addRule(15);
        params.topMargin = (int) getResources().getDimension(R.dimen.SPIRIT_LEVEL_VERT_MARGIN_BTW_TOP_DOTS_AND_MID_BASE);
        params.bottomMargin = (int) getResources().getDimension(R.dimen.SPIRIT_LEVEL_VERT_MARGIN_BTW_TOP_DOTS_AND_MID_BASE);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mBubbleImage.setLayoutParams(params);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false)) {
            this.mSensorManager.unregisterListener(this);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
        }
    }

    protected void onResume() {
        super.onResume();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false)) {
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
            this.mBubbleSoundTimerHandler.postDelayed(this.mBubbleSoundTimerTask, (long) this.mBubbleSoundPeriod);
        }
        this.mBubbleImage.setImageResource(R.drawable.circle_white);
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.txt_c_reading_degree.setText("~");
        } else if (unit.equals("1")) {
            this.txt_c_reading_degree.setText("rad");
        } else if (unit.equals("2")) {
            this.txt_c_reading_degree.setText("gra");
        } else {
            this.txt_c_reading_degree.setText("%");
        }
        for (int i = 0; i < 8; i++) {
            this.mCalibration[i] = (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(String.format(Locale.getDefault(), SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_CALIB_KEY_TEMPLATE, new Object[]{Integer.valueOf(i)}), 0.0f);
        }
    }

    public void onClick(View source) {
        if (source.getId() == 1) {
//            onButtonMenu();
        } else if (source.getId() == 2) {
            onButtonSettings();
        } else if (source.getId() == 3) {
            onButtonLock();
        } else if (source.getId() == 4) {
            onButtonCalibration();
        } else if (source.getId() == 0) {
        }
    }

    protected void onButtonSettings() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
    }

    private void onButtonLock() {
        boolean z;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(2, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false);
        if (isLocked) {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_unlock)).getBitmap());
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBubbleSoundTimerHandler.postDelayed(this.mBubbleSoundTimerTask, (long) this.mBubbleSoundPeriod);
        } else {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_lock)).getBitmap());
            this.mSensorManager.unregisterListener(this);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        String str = SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY;
        if (isLocked) {
            z = false;
        } else {
            z = true;
        }
        editor.putBoolean(str, z);
        editor.apply();
    }

    private void onButtonCalibration() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        int calibType = AccelerometerUtility.getCalibTypeWithRotation(((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation(), this.mAngleOzy, this.mAngleOzx, this.mAngleOxy);
        this.mCalibration[calibType] = -this.mOutputAngle;
        String calibKey = String.format(Locale.getDefault(), SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_CALIB_KEY_TEMPLATE, new Object[]{Integer.valueOf(calibType)});
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putFloat(calibKey, (float) this.mCalibration[calibType]);
        editor.apply();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            double y = (double) event.values[1];
            double z = (double) (-event.values[2]);
            this.mGravityX = (this.mFilteringFactor * ((double) event.values[0])) + (this.mGravityX * (1.0d - this.mFilteringFactor));
            this.mGravityY = (this.mFilteringFactor * y) + (this.mGravityY * (1.0d - this.mFilteringFactor));
            this.mGravityZ = (this.mFilteringFactor * z) + (this.mGravityZ * (1.0d - this.mFilteringFactor));
            this.mAngleOxy = Math.atan2(-this.mGravityY, -this.mGravityX);
            this.mAngleOzy = Math.atan2(this.mGravityZ, this.mGravityY);
            this.mAngleOzx = Math.atan2(this.mGravityZ, this.mGravityX);
            this.mAngleOxy = AccelerometerUtility.validateAngle(this.mAngleOxy);
            this.mAngleOzy = AccelerometerUtility.validateAngle(this.mAngleOzy);
            this.mAngleOzx = AccelerometerUtility.validateAngle(this.mAngleOzx);
            this.mAngleOxy = AccelerometerUtility.to180(this.mAngleOxy);
            this.mAngleOzy = AccelerometerUtility.to180(this.mAngleOzy);
            this.mAngleOzx = AccelerometerUtility.to180(this.mAngleOzx);
            updateBubblePosition();
        }
    }

    @SuppressLint({"DefaultLocale"})
    private void updateBubblePosition() {
        int orientation = ((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation();
        double angle = 0.0d;
        boolean fullBubble = true;
        if (orientation == 0) {
            if (Math.abs(this.mAngleOzy) < 0.7853981633974483d) {
                angle = this.mAngleOzx;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 0);
                fullBubble = false;
            }
        } else if (orientation == 2) {
            if (Math.abs(this.mAngleOzy) < 0.7853981633974483d) {
                angle = -this.mAngleOzx;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 2);
                fullBubble = false;
            }
        } else if (orientation == 1) {
            if (Math.abs(this.mAngleOzx) < 0.7853981633974483d) {
                angle = this.mAngleOzy;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 1);
                fullBubble = false;
            }
        } else if (orientation == 3) {
            if (Math.abs(this.mAngleOzx) < 0.7853981633974483d) {
                angle = -this.mAngleOzy;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 3);
                fullBubble = false;
            }
        }
        this.mOutputAngle = angle;
        angle += this.mCalibration[AccelerometerUtility.getCalibTypeWithRotation(orientation, this.mAngleOzy, this.mAngleOzx, this.mAngleOxy)];
        double convertedUnitAngle = angle;
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            convertedUnitAngle = (double) MiscUtility.radian2Degree((float) angle);
        } else {
            if (unit.equals("2")) {
                convertedUnitAngle = (double) MiscUtility.radian2Gradian((float) angle);
            } else {
                if (unit.equals("3")) {
                    convertedUnitAngle = (double) MiscUtility.radian2Slope((float) angle);
                }
            }
        }
        this.txt_c_reading.setText(String.format(Locale.getDefault(), "%.1f", new Object[]{Double.valueOf(convertedUnitAngle)}));
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY, "0").equals("1")) {
            this.mBubbleImage.setImageResource(fullBubble ? R.drawable.circle_white : R.drawable.circle_white);
        }
        int tubeLengthBound = this.cimg.getWidth() / 2;
        int newBubbleTranslation = (int) ((((double) tubeLengthBound) / Math.sin((double) (((float) PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsActivity.SettingsKey.SETTINGS_SPIRIT_LEVEL_SENSITIVITY_KEY, 80)) / 100.0f))) * Math.sin(angle));
        if (Math.abs(newBubbleTranslation) > tubeLengthBound) {
            newBubbleTranslation = MiscUtility.getSign(newBubbleTranslation) * tubeLengthBound;
        }
        Animation translateAnimation = new TranslateAnimation((float) this.mBubbleCurPos, (float) newBubbleTranslation, 0.0f, 0.0f);
        translateAnimation.setDuration(100);
        translateAnimation.setFillAfter(true);
        this.mBubbleImage.startAnimation(translateAnimation);
        this.mBubbleCurPos = newBubbleTranslation;
        float distance = Math.abs(((float) newBubbleTranslation) / ((float) tubeLengthBound));
        this.mBubbleSoundPeriod = (int) (400.0f + (1600.0f * distance));
        this.mBubbleSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE - distance > 0.5f ? 0.5f : TextTrackStyle.DEFAULT_FONT_SCALE - distance;
    }

    public class ConstantId {
        public static final int TIMER_PERIOD_MAX = 2000;
        public static final int TIMER_PERIOD_MIN = 400;
    }

    public class ControlId {
        public static final int BASE_IMAGE_ID = 5;
        public static final int BASE_RIM_IMAGE_ID = 6;
        public static final int BKG_THEME_IMAGE_ID = 11;
        public static final int BUBBLE_IMAGE_ID = 13;
        public static final int CALIBRATION_BUTTON_ID = 4;
        public static final int DITCH_IMAGE_ID = 9;
        public static final int LED_SCREEN_IMAGE_ID = 15;
        public static final int LOCK_BUTTON_ID = 3;
        public static final int LOWER_DOTS_IMAGE_ID = 8;
        public static final int MENU_BUTTON_ID = 1;
        public static final int OUTPUT_TEXT_ID = 16;
        public static final int OUTPUT_UNIT_TEXT_ID = 17;
        public static final int PIPE_IMAGE_ID = 10;
        public static final int RING_IMAGE_ID = 14;
        public static final int SETTINGS_BUTTON_ID = 2;
        public static final int TUBE_IMAGE_ID = 12;
        public static final int UPGRADE_BUTTON_ID = 0;
        public static final int UPPER_DOTS_IMAGE_ID = 7;
    }
}

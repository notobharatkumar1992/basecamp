package com.meritalberta.Activities;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.meritalberta.AppDelegate;
import com.meritalberta.R;
import com.meritalberta.constants.Tags;

import carbon.widget.TextView;

/**
 * Created by HEENA on 07/26/2016.
 */
public class WebViewDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressbar;
    private int value = 0;
    private TextView txt_c_header;
    WebView webview;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.web_view_page);
        value = getIntent().getExtras().getInt(Tags.FROM);
        initView();
    }

    private void initView() {
        txt_c_header = (TextView) findViewById(R.id.txt_c_header);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);

        findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        webview = (WebView) findViewById(R.id.webview);
        String url = getResources().getString(R.string.app_name);
        if (AppDelegate.isValidString(getIntent().getStringExtra(Tags.website_url)))
            url = getIntent().getStringExtra(Tags.website_url);
        String header = getIntent().getStringExtra(Tags.title);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        progressbar.setVisibility(View.VISIBLE);
        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressbar.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                if (progressbar.getVisibility() == View.VISIBLE) {
                    progressbar.setVisibility(View.GONE);
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(WebViewDetailActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                if (progressbar.getVisibility() == View.VISIBLE) {
                    progressbar.setVisibility(View.GONE);
                }
            }
        });
        webview.loadUrl(url);

        if (AppDelegate.isValidString(header))
            txt_c_header.setText(header);
    }

    @Override
    protected void onDestroy() {
        if (webview != null) {
            webview.destroy();
            webview = null;
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
    }

}

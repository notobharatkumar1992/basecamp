package com.meritalberta.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ListView;

import com.meritalberta.AppDelegate;
import com.meritalberta.Async.PostAsync;
import com.meritalberta.Fragments.ContactUsFragment;
import com.meritalberta.Fragments.HomeFragment;
import com.meritalberta.Fragments.NewsFragment;
import com.meritalberta.Fragments.SettingsFragment;
import com.meritalberta.Models.PostAysnc_Model;
import com.meritalberta.R;
import com.meritalberta.Utils.Prefs;
import com.meritalberta.constants.ServerRequestConstants;
import com.meritalberta.constants.Tags;
import com.meritalberta.interfaces.OnReciveServerResponse;
import com.meritalberta.parser.JSONParser;
import com.meritalberta.spirit_level.SpiritLevelActivity2;

import org.json.JSONObject;

import java.util.ArrayList;

import SlidingPaneLayout.SlidingPaneLayout1;
import carbon.widget.EditText;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by Bharat on 12-Dec-16.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public LinearLayout ll_c_signup, ll_c_home, ll_c_contact_us, ll_c_news, ll_c_spirit_level, ll_c_settings, ll_c_twitter, ll_c_facebook, ll_c_instagram;
    public TextView txt_c_signup, txt_c_home, txt_c_contact_us, txt_c_news, txt_c_spirit_level, txt_c_settings, txt_c_twitter, txt_c_facebook, txt_c_instagram;

    public android.widget.LinearLayout side_panel;
    public SlidingPaneLayout1 mSlidingPaneLayout;
    public SlideMenuClickListener menuClickListener = new SlideMenuClickListener();
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;

    public Handler mHandler;
    public Prefs prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        AppDelegate.LogT("test_size_used => " + getString(R.string.test_size_used));
        setContentView(R.layout.main_activity);
        prefs = new Prefs(MainActivity.this);
        initView();
        setHandler();
       /* if (!AppDelegate.isValidString(prefs.getStringValue(Tags.is_view, ""))) {
            showAlertDialog();
        }*/
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MainActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MainActivity.this);
                        break;
                    case 2:
                        break;
                    case 4:
                        break;
                }
            }
        };
    }

    public String str_email = "";

    public void showAlertDialog() {
        /* Alert Dialog Code Start*/
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Please enter your email address to participate in contest to win prize."); //Set Alert dialog title here
        alert.setCancelable(false);

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        input.setLayoutParams(params);
        input.setPadding(AppDelegate.dpToPix(this, 25), AppDelegate.dpToPix(this, 15), AppDelegate.dpToPix(this, 25), 0);
        input.setHint("Email Address");
        input.setFocusable(true);
        input.setText(str_email);
        input.setSelection(input.length());
        input.setFocusableInTouchMode(true);
        input.setEnabled(true);
        alert.setView(input);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //You will get as string input data in this variable.
                // here we convert the input to a string and show in a toast.
                str_email = input.getText().toString();
                if (!AppDelegate.CheckEmail(str_email)) {
                    AppDelegate.showToast(MainActivity.this, getResources().getString(R.string.foremail));
                    showAlertDialog();
                } else {
                    execute_addUserApi(str_email);
                }

            } // End of onClick(DialogInterface dialog, int whichButton)
        }); //End of alert.setPositiveButton
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                prefs.putStringValue(Tags.is_view, Tags.is_view);
                dialog.cancel();
            }
        }); //End of alert.setNegativeButton
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    private void execute_addUserApi(String email_address) {
        try {
            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.email, email_address);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_id, tManager.getDeviceId());
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.ADD_USER,
                        mPostArrayList, null);
                prefs.putStringValue(Tags.is_view, Tags.is_view);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            showAlertDialog();
            // AppDelegate.showToast(this, "Please try again.");
        }
    }

    private void initView() {
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.mSlidingPaneLayout);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());
        side_panel = (android.widget.LinearLayout) findViewById(R.id.side_panel);
        ll_c_home = (LinearLayout) findViewById(R.id.ll_c_home);
        ll_c_home.setOnClickListener(this);
        ll_c_contact_us = (LinearLayout) findViewById(R.id.ll_c_contact_us);
        ll_c_contact_us.setOnClickListener(this);
        ll_c_news = (LinearLayout) findViewById(R.id.ll_c_news);
        ll_c_news.setOnClickListener(this);
        ll_c_spirit_level = (LinearLayout) findViewById(R.id.ll_c_spirit_level);
        ll_c_spirit_level.setOnClickListener(this);
        ll_c_settings = (LinearLayout) findViewById(R.id.ll_c_settings);
        ll_c_settings.setOnClickListener(this);
        ll_c_twitter = (LinearLayout) findViewById(R.id.ll_c_twitter);
        ll_c_twitter.setOnClickListener(this);
        ll_c_facebook = (LinearLayout) findViewById(R.id.ll_c_facebook);
        ll_c_facebook.setOnClickListener(this);
        ll_c_instagram = (LinearLayout) findViewById(R.id.ll_c_instagram);
        ll_c_instagram.setOnClickListener(this);
        ll_c_signup = (LinearLayout) findViewById(R.id.ll_c_signup);
        ll_c_signup.setOnClickListener(this);
        txt_c_home = (TextView) findViewById(R.id.txt_c_home);
        txt_c_contact_us = (TextView) findViewById(R.id.txt_c_contact_us);
        txt_c_news = (TextView) findViewById(R.id.txt_c_news);
        txt_c_spirit_level = (TextView) findViewById(R.id.txt_c_spirit_level);
        txt_c_settings = (TextView) findViewById(R.id.txt_c_settings);
        txt_c_twitter = (TextView) findViewById(R.id.txt_c_twitter);
        txt_c_facebook = (TextView) findViewById(R.id.txt_c_facebook);
        txt_c_instagram = (TextView) findViewById(R.id.txt_c_instagram);
        txt_c_signup = (TextView) findViewById(R.id.txt_c_signup);
        displayView(PANEL_HOME);
    }


    public void setInitailSideBar(int value) {
        if (value == PANEL_SPIRIT_LEVEL || value == PANEL_TWITTER || value == PANEL_FACEBOOK || value == PANEL_INSTAGRAM || value == PANEL_SIGNUP)
            return;
        ll_c_home.setSelected(false);
        ll_c_contact_us.setSelected(false);
        ll_c_news.setSelected(false);
        ll_c_spirit_level.setSelected(false);
        ll_c_settings.setSelected(false);
        ll_c_twitter.setSelected(false);
        ll_c_facebook.setSelected(false);
        ll_c_instagram.setSelected(false);
        ll_c_signup.setSelected(false);

        txt_c_home.setSelected(false);
        ll_c_contact_us.setSelected(false);
        txt_c_news.setSelected(false);
        txt_c_spirit_level.setSelected(false);
        txt_c_settings.setSelected(false);
        txt_c_twitter.setSelected(false);
        txt_c_facebook.setSelected(false);
        txt_c_instagram.setSelected(false);
        txt_c_signup.setSelected(false);
        switch (value) {
            case PANEL_HOME:
                ll_c_home.setSelected(true);
                txt_c_home.setSelected(true);
                break;
            case PANEL_CONTACT_US:
                ll_c_contact_us.setSelected(true);
                txt_c_contact_us.setSelected(true);
                break;
            case PANEL_NEWS:
                txt_c_news.setSelected(true);
                ll_c_news.setSelected(true);
                break;
            case PANEL_SPIRIT_LEVEL:
                txt_c_spirit_level.setSelected(true);
                ll_c_spirit_level.setSelected(true);
                break;
            case PANEL_SETTINGS:
                txt_c_settings.setSelected(true);
                ll_c_settings.setSelected(true);
                break;
            case PANEL_TWITTER:
                txt_c_twitter.setSelected(true);
                ll_c_twitter.setSelected(true);
                break;
            case PANEL_FACEBOOK:
                txt_c_facebook.setSelected(true);
                ll_c_facebook.setSelected(true);
                break;
            case PANEL_INSTAGRAM:
                ll_c_instagram.setSelected(true);
                txt_c_instagram.setSelected(true);
                break;
            case PANEL_SIGNUP:
                ll_c_signup.setSelected(true);
                txt_c_signup.setSelected(true);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_c_home:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment))
                    menuClickListener.onItemClick(null, v, PANEL_HOME, PANEL_HOME);
                break;
            case R.id.ll_c_contact_us:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof ContactUsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_CONTACT_US, PANEL_CONTACT_US);
                break;
            case R.id.ll_c_news:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof NewsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_NEWS, PANEL_NEWS);
                break;
            case R.id.ll_c_spirit_level:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_SPIRIT_LEVEL, PANEL_SPIRIT_LEVEL);
                break;
            case R.id.ll_c_settings:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof SettingsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_SETTINGS, PANEL_SETTINGS);
                break;
            case R.id.ll_c_twitter:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_TWITTER, PANEL_TWITTER);
                break;
            case R.id.ll_c_facebook:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_FACEBOOK, PANEL_FACEBOOK);
                break;
            case R.id.ll_c_instagram:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_INSTAGRAM, PANEL_INSTAGRAM);
                break;
            case R.id.ll_c_signup:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_SIGNUP, PANEL_SIGNUP);
                break;
        }
    }

    public static final int PANEL_HOME = 0, PANEL_CONTACT_US = 1, PANEL_NEWS = 2, PANEL_SPIRIT_LEVEL = 3, PANEL_SETTINGS = 4, PANEL_TWITTER = 5, PANEL_FACEBOOK = 6, PANEL_INSTAGRAM = 7, PANEL_SIGNUP = 8;

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(MainActivity.this, getString(R.string.time_out));
            return;
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ADD_USER)) {
            parseAddUserResponse(result);
        }
    }

    private void parseAddUserResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(MainActivity.this, JSONParser.getString(jsonObject, "Message"));
            } else {
                AppDelegate.showToast(MainActivity.this, JSONParser.getString(jsonObject, "Message"));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }


    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(position);
                }
            }, 600);
        }
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(MainActivity.this);
        setInitailSideBar(position);
        Fragment fragment = null;
        switch (position) {
            case PANEL_HOME:
                fragment = new HomeFragment();
                break;
            case PANEL_CONTACT_US:
                fragment = new ContactUsFragment();
                break;
            case PANEL_NEWS:
                fragment = new NewsFragment();
                break;
            case PANEL_SPIRIT_LEVEL:
                startActivity(new Intent(this, SpiritLevelActivity2.class));
                break;
            case PANEL_SETTINGS:
                fragment = new SettingsFragment();
                break;
            case PANEL_TWITTER:
                Intent intent = new Intent(this, WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.TWITTER);
                intent.putExtra(Tags.title, Tags.twitter);
                startActivity(intent);
//                AppDelegate.openUrl(this, ServerRequestConstants.TWITTER);
                break;
            case PANEL_FACEBOOK:
                intent = new Intent(this, WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.FACEBOOK);
                intent.putExtra(Tags.title, Tags.facebook);
                startActivity(intent);
//                AppDelegate.openUrl(this, ServerRequestConstants.FACEBOOK);
                break;
            case PANEL_INSTAGRAM:
                intent = new Intent(this, WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.INSTAGRAM);
                intent.putExtra(Tags.title, Tags.instagram);
                startActivity(intent);
//                AppDelegate.openUrl(this, ServerRequestConstants.INSTAGRAM);
                break;
            case PANEL_SIGNUP:
                showAlertDialog();
//                AppDelegate.openUrl(this, ServerRequestConstants.INSTAGRAM);
                break;
        }
        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment, R.id.main_content);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener {
        @Override
        public void onPanelOpened(View panel) {
            if (!isSlideOpen) {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(MainActivity.this);
        }

        @Override
        public void onPanelClosed(View panel) {
            if (isSlideOpen) {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset) {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }

    public void toggleSlider() {
        if (mSlidingPaneLayout != null)
            if (!mSlidingPaneLayout.isOpen()) {
                mSlidingPaneLayout.openPane();
            } else {
                mSlidingPaneLayout.closePane();
            }
    }

    public void whileSlide(int view) {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }

    @Override
    public void onBackPressed() {
        if (mSlidingPaneLayout.isOpen())
            mSlidingPaneLayout.closePane();
        else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment)
            finish();
        else super.onBackPressed();
    }
}

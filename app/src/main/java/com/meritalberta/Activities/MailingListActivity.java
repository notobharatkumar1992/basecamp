package com.meritalberta.Activities;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.meritalberta.AppDelegate;
import com.meritalberta.Async.PostAsync;
import com.meritalberta.Models.PostAysnc_Model;
import com.meritalberta.R;
import com.meritalberta.Utils.Prefs;
import com.meritalberta.constants.ServerRequestConstants;
import com.meritalberta.constants.Tags;
import com.meritalberta.interfaces.OnReciveServerResponse;
import com.meritalberta.parser.JSONParser;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 13-Dec-16.
 */

public class MailingListActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public EditText et_company_name, et_first_name, et_last_name, et_email, et_postal_code;
    TextView txt_c_title;
    private Handler mHandler;
    private Prefs prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.mailing_list_activity);
        setHandler();
        initView();
        prefs = new Prefs(this);
    }

    private void initView() {
        et_company_name = (EditText) findViewById(R.id.et_company_name);
        et_company_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_postal_code = (EditText) findViewById(R.id.et_postal_code);
        et_postal_code.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_title.setText(getString(R.string.mailing_list));
        findViewById(R.id.txt_c_agree).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_agree:
                checkValidation();
                break;
            case R.id.img_c_back:
                finish();
                break;
            case R.id.txt_c_back:
                finish();
                break;
        }
    }

    private void checkValidation() {
       /* if (!AppDelegate.isValidString(et_company_name.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forcompany));
        } else if (!AppDelegate.isValidString(et_first_name.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forname));
        } else if (!AppDelegate.isValidString(et_last_name.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forlastname));
        } else*/
        if (!AppDelegate.CheckEmail(et_email.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.foremail));
        }/* else if (!AppDelegate.isValidString(et_postal_code.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forpostal));
        } */ else if (AppDelegate.haveNetworkConnection(this, false)) {
            mHandler.sendEmptyMessage(1);
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MailingListActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MailingListActivity.this);
                        break;
                    case 1:
                        execute_MailingList();
                        break;
                    case 2:
                        break;

                }
            }
        };
    }

    private void execute_MailingList() {
        try {
            if (AppDelegate.haveNetworkConnection(MailingListActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(MailingListActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(MailingListActivity.this).setPostParamsSecond(mPostArrayList, Tags.company_name, et_company_name.getText().toString());
                AppDelegate.getInstance(MailingListActivity.this).setPostParamsSecond(mPostArrayList, Tags.first_name, et_first_name.getText().toString());
                AppDelegate.getInstance(MailingListActivity.this).setPostParamsSecond(mPostArrayList, Tags.last_name, et_last_name.getText().toString());
                AppDelegate.getInstance(MailingListActivity.this).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());
                AppDelegate.getInstance(MailingListActivity.this).setPostParamsSecond(mPostArrayList, Tags.postal_code, et_postal_code.getText().toString());
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(MailingListActivity.this, this, ServerRequestConstants.MAILING_LIST, mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(MailingListActivity.this, getString(R.string.try_again));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getString(R.string.time_out));
            return;
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.MAILING_LIST)) {
            parseMailingList(result);
        }
    }

    private void parseMailingList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(MailingListActivity.this, jsonObject.getString(Tags.Message));
                finish();
            } else {
                AppDelegate.showAlert(this, getResources().getString(R.string.alert), JSONParser.getString(jsonObject, Tags.Message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

    }
}

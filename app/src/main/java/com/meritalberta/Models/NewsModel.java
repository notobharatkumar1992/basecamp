package com.meritalberta.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 14-Dec-16.
 */

public class NewsModel implements Parcelable {
    public int status;
    public String image, title, content, link;

    protected NewsModel(Parcel in) {
        status = in.readInt();
        image = in.readString();
        title = in.readString();
        content = in.readString();
        link = in.readString();
    }

    @Override
    public String toString() {
        return "NewsModel{" +
                "status=" + status +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", link='" + link + '\'' +
                '}';
    }

    public NewsModel() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeString(image);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(link);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewsModel> CREATOR = new Creator<NewsModel>() {
        @Override
        public NewsModel createFromParcel(Parcel in) {
            return new NewsModel(in);
        }

        @Override
        public NewsModel[] newArray(int size) {
            return new NewsModel[size];
        }
    };
}

package com.meritalberta.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meritalberta.Activities.MailingListActivity;
import com.meritalberta.Activities.MainActivity;
import com.meritalberta.Activities.MyInfoActivity;
import com.meritalberta.R;

/**
 * Created by Bharat on 13-Dec-16.
 */

public class SettingsFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_SETTINGS);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_SETTINGS);
    }

    private void initView(View view) {
        view.findViewById(R.id.rl_my_info).setOnClickListener(this);
        view.findViewById(R.id.rl_mailing_list).setOnClickListener(this);
        view.findViewById(R.id.img_c_menu).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_my_info:
                startActivity(new Intent(getActivity(), MyInfoActivity.class));
                break;
            case R.id.rl_mailing_list:
                startActivity(new Intent(getActivity(), MailingListActivity.class));
                break;
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }
}

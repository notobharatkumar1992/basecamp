package com.meritalberta.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.SoundPool;

import com.meritalberta.AppDelegate;
import com.google.android.gms.cast.TextTrackStyle;

import java.io.IOException;
import java.util.HashMap;

public class SoundUtility {
    private static SoundUtility mInstance;
    private Context mContext;
    private MediaPlayer mPreviewPlayer;
    private HashMap<Integer, String> mSoundPathMap;
    private SoundPool mSoundPool;
    private HashMap<Integer, Integer> mSoundPoolMap;
    private HashMap<Integer, Integer> mStreamIdMap;

    public class SoundId {
        public static final int BEEP = 1;
        public static final int LOCK = 2;
        public static final int MENU_BUTTON_OK_CLICKED = 6;
        public static final int MENU_ITEM_ROTATED = 5;
        public static final int MENU_SHUTTER_MOVED = 7;
        public static final int MENU_SLIDED = 8;
        public static final int SLIDE = 3;
        public static final int TICK = 4;
    }

    public class SoundVolume {
        public static final int MAX_VOLUME = 1;
        public static final int MIN_VOLUME = 0;
    }

    static {
        mInstance = null;
    }

    private SoundUtility() {
        this.mPreviewPlayer = new MediaPlayer();
    }

    public static SoundUtility getInstance() {
        if (mInstance == null) {
            mInstance = new SoundUtility();
        }
        return mInstance;
    }

    @SuppressLint({"UseSparseArrays"})
    public void initSounds(Context theContext) {
        this.mContext = theContext;
        this.mSoundPool = new SoundPool(8, 3, 0);
        this.mSoundPoolMap = new HashMap();
        this.mSoundPathMap = new HashMap();
        this.mStreamIdMap = new HashMap();
    }

    public void addSound(int Index, int SoundID) {
        this.mSoundPoolMap.put(Integer.valueOf(Index), Integer.valueOf(this.mSoundPool.load(this.mContext, SoundID, 1)));
    }

    public void addSound(int Index, String SoundPath) {
        try {
            this.mSoundPoolMap.put(Integer.valueOf(Index), Integer.valueOf(this.mSoundPool.load(this.mContext.getAssets().openFd(SoundPath), 1)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeSound(int Index) {
        this.mSoundPool.unload(((Integer) this.mSoundPoolMap.get(Integer.valueOf(Index))).intValue());
        this.mSoundPoolMap.remove(Integer.valueOf(Index));
    }

    public void loadAllSounds() {
        this.mSoundPathMap.put(Integer.valueOf(1), "sounds/beep.mp3");
        this.mSoundPathMap.put(Integer.valueOf(2), "sounds/lock.mp3");
        this.mSoundPathMap.put(Integer.valueOf(3), "sounds/slide.mp3");
        this.mSoundPathMap.put(Integer.valueOf(4), "sounds/tick.mp3");
        this.mSoundPathMap.put(Integer.valueOf(5), "sounds/menu/item_rotated.mp3");
        this.mSoundPathMap.put(Integer.valueOf(6), "sounds/menu/button_ok_clicked.mp3");
        this.mSoundPathMap.put(Integer.valueOf(7), "sounds/menu/shutter_moved.mp3");
        this.mSoundPathMap.put(Integer.valueOf(8), "sounds/menu/slided.mp3");
        addSound(1, (String) this.mSoundPathMap.get(Integer.valueOf(1)));
        addSound(2, (String) this.mSoundPathMap.get(Integer.valueOf(2)));
        addSound(3, (String) this.mSoundPathMap.get(Integer.valueOf(3)));
        addSound(4, (String) this.mSoundPathMap.get(Integer.valueOf(4)));
        addSound(5, (String) this.mSoundPathMap.get(Integer.valueOf(5)));
        addSound(6, (String) this.mSoundPathMap.get(Integer.valueOf(6)));
        addSound(7, (String) this.mSoundPathMap.get(Integer.valueOf(7)));
        addSound(8, (String) this.mSoundPathMap.get(Integer.valueOf(8)));
    }

    public void playSound(int index, float volume) {
        try {
            if (!this.mSoundPoolMap.containsKey(Integer.valueOf(index))) {
                addSound(index, (String) this.mSoundPathMap.get(Integer.valueOf(index)));
            }
            this.mSoundPool.play(((Integer) this.mSoundPoolMap.get(Integer.valueOf(index))).intValue(), volume, volume, 1, 0, TextTrackStyle.DEFAULT_FONT_SCALE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void playSoundLoop(int index, float volume) {
        try {
            if (!this.mSoundPoolMap.containsKey(Integer.valueOf(index))) {
                addSound(index, (String) this.mSoundPathMap.get(Integer.valueOf(index)));
            }
            int streamID = this.mSoundPool.play(((Integer) this.mSoundPoolMap.get(Integer.valueOf(index))).intValue(), volume, volume, 1, -1, TextTrackStyle.DEFAULT_FONT_SCALE);
            if (streamID != 0) {
                this.mStreamIdMap.put(Integer.valueOf(index), Integer.valueOf(streamID));
            } else {
                AppDelegate.LogT("Play sound fail: " + ((String) this.mSoundPathMap.get(Integer.valueOf(index))));
            }
        } catch (NullPointerException e) {
            AppDelegate.LogE(e);
        }
    }

    public void stopSound(int index) {
        try {
            this.mSoundPool.stop(((Integer) this.mStreamIdMap.get(Integer.valueOf(index))).intValue());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void cleanup() {
        this.mSoundPool.release();
        this.mSoundPool = null;
        this.mSoundPoolMap.clear();
        this.mSoundPathMap.clear();
        this.mStreamIdMap.clear();
        this.mPreviewPlayer.release();
        mInstance = null;
    }

    public void previewSound(String sSoundPath) {
        try {
            this.mPreviewPlayer.reset();
            AssetFileDescriptor descriptor = this.mContext.getAssets().openFd(sSoundPath);
            this.mPreviewPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            this.mPreviewPlayer.prepare();
            this.mPreviewPlayer.setLooping(false);
            this.mPreviewPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
            this.mPreviewPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

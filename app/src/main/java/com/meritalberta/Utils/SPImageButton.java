package com.meritalberta.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.NinePatchDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.v4.content.ContextCompat;
import android.widget.ImageButton;


public class SPImageButton extends ImageButton {
    boolean mStateOn;

    public SPImageButton(Context context) {
        super(context);
        this.mStateOn = false;
        setBackgroundColor(0);
        setSoundEffectsEnabled(false);
        setPadding(0, 0, 0, 0);
    }

    public void setBackgroundBitmapId(int resId) {
        setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(getContext(), resId)).getBitmap());
    }

    public void setBackgroundBitmap(Bitmap bmp) {
        setBackgroundBitmap(Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight() / 2), Bitmap.createBitmap(bmp, 0, bmp.getHeight() / 2, bmp.getWidth(), bmp.getHeight() / 2));
    }

    public void setBackgroundBitmap(Bitmap bmp1, Bitmap bmp2) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{16842919}, new BitmapDrawable(getResources(), bmp2));
        states.addState(new int[0], new BitmapDrawable(getResources(), bmp1));
        setBackgroundBackwardCompatible(states);
    }

    public void setBackgroundNinePatchBitmap(Bitmap bmp1, Bitmap bmp2) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{16842919}, new NinePatchDrawable(getResources(), bmp1, bmp1.getNinePatchChunk(), new Rect(), null));
        states.addState(new int[0], new NinePatchDrawable(getResources(), bmp2, bmp2.getNinePatchChunk(), new Rect(), null));
        setBackgroundBackwardCompatible(states);
    }

    public void setBackgroundAutoFlipNinePatchBitmap(Bitmap bmp) {
        StateListDrawable states = new StateListDrawable();
        int[] iArr = new int[]{16842919};
        states.addState(iArr, new NinePatchDrawable(getResources(), ImageUtility.flipVertical(bmp), bmp.getNinePatchChunk(), new Rect(), null));
        states.addState(new int[0], new NinePatchDrawable(getResources(), bmp, bmp.getNinePatchChunk(), new Rect(), null));
        setBackgroundBackwardCompatible(states);
    }

    public void setBackgroundAutoFlipBitmap(Bitmap bmp) {
        StateListDrawable states = new StateListDrawable();
        int[] iArr = new int[]{16842919};
        states.addState(iArr, new BitmapDrawable(getResources(), ImageUtility.flipVertical(bmp)));
        states.addState(new int[0], new BitmapDrawable(getResources(), bmp));
        setBackgroundBackwardCompatible(states);
    }

  /*  public void setBackgroundAutoTransparentNinePatchBitmap(Bitmap bmp) {
        StateListDrawable states = new StateListDrawable();
        int[] iArr = new int[]{16842919};
        states.addState(iArr, new NinePatchDrawable(getResources(), ImageUtility.createBitmap(bmp, 100), bmp.getNinePatchChunk(), new Rect(), null));
        states.addState(new int[0], new NinePatchDrawable(getResources(), ImageUtility.createBitmap(bmp, SeismometerGraphView.HISTORY_SIZE_PER_PAGE), bmp.getNinePatchChunk(), new Rect(), null));
        setBackgroundBackwardCompatible(states);
    }*/

    public void setBackgroundAutoTransparentBitmap(Bitmap bmp) {
        StateListDrawable states = new StateListDrawable();
        int[] iArr = new int[]{16842919};
        states.addState(iArr, new BitmapDrawable(getResources(), ImageUtility.createBitmap(bmp, 100)));
        states.addState(new int[0], new BitmapDrawable(getResources(), bmp));
        setBackgroundBackwardCompatible(states);
    }

    protected void setBackgroundBackwardCompatible(StateListDrawable states) {
        if (VERSION.SDK_INT < 16) {
            setBackgroundDrawable(states);
        } else {
            setBackground(states);
        }
    }

    public void setStateOn(boolean on) {
        this.mStateOn = on;
    }

    public boolean isStateOn() {
        return this.mStateOn;
    }
}

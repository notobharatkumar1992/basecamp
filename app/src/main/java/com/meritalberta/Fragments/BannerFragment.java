package com.meritalberta.Fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.meritalberta.Activities.WebViewDetailActivity;
import com.meritalberta.AppDelegate;
import com.meritalberta.Models.NewsModel;
import com.meritalberta.R;
import com.meritalberta.constants.Tags;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import carbon.widget.TextView;

public class BannerFragment extends Fragment {

    private View rootview;
    private Bundle bundle;
    ImageView image;
    TextView txt_c_news_title;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true)/*.showImageForEmptyUri(R.drawable)*/.
                    build();
    private NewsModel newsModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.banner_image, container, false);
        bundle = getArguments();
        newsModel = bundle.getParcelable(Tags.DATA);
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = (ImageView) rootview.findViewById(R.id.img_c_banner_image);
        txt_c_news_title = (TextView) rootview.findViewById(R.id.txt_c_news_title);

        if (newsModel != null) {
            if (AppDelegate.isValidString(newsModel.title))
                txt_c_news_title.setText(newsModel.title);
            if (AppDelegate.isValidString(newsModel.image))
                imageLoader.loadImage(newsModel.image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        image.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), WebViewDetailActivity.class);
                    intent.putExtra(Tags.website_url, newsModel.link);
                    intent.putExtra(Tags.title, newsModel.title);
                    startActivity(intent);
//                    AppDelegate.openURL(getActivity(), newsModel.link);
                }
            });
        }
    }

}


package com.meritalberta.constants;

public class ServerRequestConstants {
    /**
     * (All Constants that were used in server connection are declared)
     */
    /*
     * Post Parameter Type Constants
	 */
    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_402 = "402";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";

    public static final String Key_PostStrValue = "String";
    public static final String Key_PostDoubleValue = "double";
    public static final String Key_PostintValue = "int";
    public static final String Key_PostbyteValue = "byte";
    public static final String Key_PostFileValue = "File";
    public static final String Key_PostStringArrayValue = "String[]";
    public static final String Key_PostStringArrayValue1 = "String[]";
    public static final String MERIT_BLOCKS_GAME = "http://www.chupamobile.com/unity-arcade/tetris-pro-ios-android-15435";
    public static final String BASE_URL = "https://www.meritalberta.com/";
    public static final String TWITTER = "https://twitter.com/Merit_AB";
    public static final String FACEBOOK = "https://m.facebook.com/meritalberta/";
    public static final String INSTAGRAM = "https://www.instagram.com/meritalberta/";
    public static final String NEWS_LIST = BASE_URL + "api/news.php";
    public static final String MAILING_LIST = BASE_URL + "api/mailing.php";
    public static final String MEMBER_SERVICE = BASE_URL + "member-services/#Content";
    public static final String NEWS = BASE_URL + "news/#news";
    public static final String PERKS = BASE_URL + "member-services/perks/#Content";
    public static final String BENEFITS = BASE_URL + "member-services/mercon-benefit-plan/#Content";
    public static final String TRAINING = BASE_URL + "training/#Content";
    public static final String ADD_USER = BASE_URL + "api/app_user.php";
}


package com.meritalberta.Utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.view.InputDeviceCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.meritalberta.BuildConfig;
import com.meritalberta.R;
import com.meritalberta.spirit_level.SpiritLevelActivity2;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener, OnPreferenceClickListener {

    static void onCalibrate(Context context, int type, Preference calibrationPref) {
        String msg;
        View view;
        String title = context.getResources().getString(R.string.IDS_CALIBRATION);
        if (type == 0) {
            msg = context.getResources().getString(R.string.IDS_CALIBRATION_MESSAGE_1);
        } else if (type == 1) {
            msg = context.getResources().getString(R.string.IDS_CALIBRATION_MESSAGE_2);
        } else {
            msg = context.getResources().getString(R.string.IDS_CALIBRATION_MESSAGE_3);
        }
        String done = context.getResources().getString(R.string.IDS_DONE);
        String close = context.getResources().getString(R.string.IDS_CLOSE);
        EditText input = new EditText(context);
        input.setInputType(InputDeviceCompat.SOURCE_MOUSE);
        Builder builder = new Builder(context);
        if (type <= 1) {
            view = input;
        } else {
            view = null;
        }
        AlertDialog d = builder.setView(view).setTitle(title).setMessage(msg).setPositiveButton(done, null).setNegativeButton(close, null).create();
        d.setOnShowListener(new AnonymousClass2(d, input, type, context, calibrationPref));
        d.show();
    }

    protected void onCreate(Bundle savedInstanceState) {
        int i;
        super.onCreate(savedInstanceState);
        String action = getIntent().getAction();
        if (action != null) {
            if (action.equals("settings_general")) {
                addPreferencesFromResource(R.xml.settings_general);
            }
        }
        if (action != null) {
            if (action.equals("settings_protractor")) {
                addPreferencesFromResource(R.xml.settings_protractor);
                Preference primaryUnitPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY);
                primaryUnitPref.setSummary(((ListPreference) primaryUnitPref).getEntry());
                Preference secondaryUnitPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY);
                secondaryUnitPref.setSummary(((ListPreference) secondaryUnitPref).getEntry());
                Preference unitToSnapPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY);
                unitToSnapPref.setSummary(((ListPreference) unitToSnapPref).getEntry());
                Preference sensitivityPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY);
                sensitivityPref.setSummary(((ListPreference) sensitivityPref).getEntry());
                loadProtractorSnappingListAccordingTo(((ListPreference) unitToSnapPref).getValue());
                updateProtractorSnappingControls();
            }
        }

        if (action != null) {
            if (action.equals("settings_spirit_level")) {
                addPreferencesFromResource(R.xml.settings_spirit_level);
                Preference unitPref = findPreference(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY);
                unitPref.setSummary(((ListPreference) unitPref).getEntry());
                Preference themePref = findPreference(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY);
                themePref.setSummary(((ListPreference) themePref).getEntry());
            }
        }




        if (VERSION.SDK_INT < 11) {
            addPreferencesFromResource(R.xml.preference_headers_legacy);
        }
    }

    @SuppressLint({"NewApi"})
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preference_headers, target);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY) || key.equals(SettingsKey.SETTINGS_SURFACE_LEVEL_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SURFACE_LEVEL_THEME_KEY) || key.equals(SettingsKey.SETTINGS_PLUMB_BOB_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SEISMOMETER_SCALE_TYPE_KEY) || key.equals(SettingsKey.SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY) || key.equals(SettingsKey.SETTINGS_SEISMOMETER_UPDATE_FREQUENCY_KEY) || key.equals(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY) || key.startsWith("SETTINGS_TIMER_DEFAULT_DURATION") || key.equals(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_COMPASS_HEADING_KEY) || key.equals(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY) || key.equals(SettingsKey.SETTINGS_ALTIMETER_UNIT_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
        } else if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            loadProtractorSnappingListAccordingTo(getListPreferenceValue(key));
        } else if (key.equals(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            loadRulerSnappingListAccordingTo(getListPreferenceValue(key));
        } else if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            Editor editor = sharedPreferences.edit();
            String unitToSnap = getListPreferenceValue(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY);
            String snapValue = ((ListPreference) findPreference(key)).getValue();
            if (unitToSnap.equals("0")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("1")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("2")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("3")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY, snapValue);
            }
            editor.apply();
        } else if (key.equals(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            Editor editor = sharedPreferences.edit();
            String unitToSnap = getListPreferenceValue(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY);
            String snapValue = ((ListPreference) findPreference(key)).getValue();
            if (unitToSnap.equals("0")) {
                editor.putString(SettingsKey.SETTINGS_RULER_CENTIMETER_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("1")) {
                editor.putString(SettingsKey.SETTINGS_RULER_INCH_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("2")) {
                editor.putString(SettingsKey.SETTINGS_RULER_PIXEL_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("3")) {
                editor.putString(SettingsKey.SETTINGS_RULER_PICA_SNAP_KEY, snapValue);
            }
            editor.apply();
        } else if (key.equals(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY) || key.startsWith("SETTINGS_TIMER_NAME") || key.startsWith("SETTINGS_TIMER_MESSAGE")) {
            EditTextPreference pref = (EditTextPreference) findPreference(key);
            pref.setSummary(pref.getText());
        } else if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_KEY)) {
            updateProtractorSnappingControls();
        } else if (key.equals(SettingsKey.SETTINGS_RULER_SNAPPING_KEY)) {
            updateRulerSnappingControls();
        } else if (key.equals(SettingsKey.SETTINGS_METRONOME_FLASH_ENABLED_KEY)) {
            updateMetronomeFlashingControls();
        } else if (key.equals(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY)) {
            updateTeslameterAlertControls();
        } else if (key.equals(SettingsKey.SETTINGS_SEISMOMETER_ALARM_SOUND_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/seismometer/alarm/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        } else if (key.equals(SettingsKey.SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY) || key.equals(SettingsKey.SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY) || key.equals(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/metronome/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        } else if (key.equals(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/teslameter/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        } else if (key.startsWith("SETTINGS_TIMER_ALARM_SOUND")) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/timer/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        }
    }

    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

    public void onResume() {
        super.onResume();
        if (VERSION.SDK_INT < 11) {
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }
    }

    public void onPause() {
        if (VERSION.SDK_INT < 11) {
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }
        super.onPause();
    }

    private void loadProtractorSnappingListAccordingTo(String unitToSnap) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Preference pref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY);
        String snappingSensitivityValue = null;
        String[] sensitivityEntries = null;
        String[] sensitivityValues = null;
        if (unitToSnap.equals("0")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.angle_degree_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_degree_sensitivity_values);
        } else if (unitToSnap.equals("1")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, "0.01");
            sensitivityEntries = getResources().getStringArray(R.array.angle_radian_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_radian_sensitivity_values);
        } else if (unitToSnap.equals("2")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.angle_gradian_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_gradian_sensitivity_values);
        } else if (unitToSnap.equals("3")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY, "0.0277777777777778");
            sensitivityEntries = getResources().getStringArray(R.array.angle_revolution_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_revolution_sensitivity_values);
        }
        ((ListPreference) pref).setEntries(sensitivityEntries);
        ((ListPreference) pref).setEntryValues(sensitivityValues);
        pref.setSummary(sensitivityEntries[Arrays.asList(sensitivityValues).indexOf(snappingSensitivityValue)]);
    }

    private void loadRulerSnappingListAccordingTo(String unitToSnap) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Preference pref = findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY);
        String snappingSensitivityValue = null;
        String[] sensitivityEntries = null;
        String[] sensitivityValues = null;
        if (unitToSnap.equals("0")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_CENTIMETER_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.length_centimeter_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_centimeter_sensitivity_values);
        } else if (unitToSnap.equals("1")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_INCH_SNAP_KEY, "0.25");
            sensitivityEntries = getResources().getStringArray(R.array.length_inch_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_inch_sensitivity_values);
        } else if (unitToSnap.equals("2")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_PIXEL_SNAP_KEY, "50");
            sensitivityEntries = getResources().getStringArray(R.array.length_pixel_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_pixel_sensitivity_values);
        } else if (unitToSnap.equals("3")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_PICA_SNAP_KEY, "0.25");
            sensitivityEntries = getResources().getStringArray(R.array.length_pica_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_pica_sensitivity_values);
        }
        ((ListPreference) pref).setEntries(sensitivityEntries);
        ((ListPreference) pref).setEntryValues(sensitivityValues);
        pref.setSummary(sensitivityEntries[Arrays.asList(sensitivityValues).indexOf(snappingSensitivityValue)]);
    }

    private String getListPreferenceEntry(String key) {
        return ((ListPreference) findPreference(key)).getEntry().toString();
    }

    private String getListPreferenceValue(String key) {
        return ((ListPreference) findPreference(key)).getValue();
    }

    private void updateProtractorSnappingControls() {
        Boolean isSnapping = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_KEY, false));
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY).setEnabled(isSnapping.booleanValue());
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY).setEnabled(isSnapping.booleanValue());
    }

    private void updateRulerSnappingControls() {
        Boolean isSnapping = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_SNAPPING_KEY, false));
        findPreference(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY).setEnabled(isSnapping.booleanValue());
        findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY).setEnabled(isSnapping.booleanValue());
    }

    private void updateMetronomeFlashingControls() {
        findPreference(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY).setEnabled(Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_METRONOME_FLASH_ENABLED_KEY, true)).booleanValue());
    }

    private void updateTeslameterAlertControls() {
        Boolean enableAlert = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY, true));
        findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY).setEnabled(enableAlert.booleanValue());
        findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY).setEnabled(enableAlert.booleanValue());
    }

    public void onAskForSupport(View v) {
        MiscUtility.sendEmail(this, "mailto:support@skypaw.com", getResources().getString(R.string.IDS_MULTI_MEASURES), BuildConfig.FLAVOR, null);
    }

    public void onReviewOnMarket(View v) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
        startActivity(intent);
    }

    public void onShareViaEmail(View v) {
        MiscUtility.sendEmail(this, "mailto:", getResources().getString(R.string.IDS_MULTI_MEASURES), getResources().getString(R.string.IDS_CHECK_OUT_THIS_APP) + "\nhttp://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName(), null);
    }

    protected boolean isValidFragment(String fragmentName) {
        return true;
    }

    /* renamed from: com.vivekwarde.measure.settings.SettingsActivity.2 */
    static class AnonymousClass2 implements OnShowListener {
        final /* synthetic */ Preference val$calibrationPref;
        final /* synthetic */ Context val$context;
        final /* synthetic */ AlertDialog val$d;
        final /* synthetic */ EditText val$input;
        final /* synthetic */ int val$type;

        AnonymousClass2(AlertDialog alertDialog, EditText editText, int i, Context context, Preference preference) {
            this.val$d = alertDialog;
            this.val$input = editText;
            this.val$type = i;
            this.val$context = context;
            this.val$calibrationPref = preference;
        }

        public void onShow(DialogInterface dialog) {
            Button doneBtn = this.val$d.getButton(-1);
            doneBtn.setOnClickListener(new AnonymousClass1(doneBtn));
        }

        /* renamed from: com.vivekwarde.measure.settings.SettingsActivity.2.1 */
        class AnonymousClass1 implements View.OnClickListener {
            final /* synthetic */ Button val$doneBtn;

            AnonymousClass1(Button button) {
                this.val$doneBtn = button;
            }

            public void onClick(View view) {
                if (view.equals(this.val$doneBtn)) {
                    String sValue = AnonymousClass2.this.val$input.getText().toString();
                    try {
                        float value = 0;
                        if (AnonymousClass2.this.val$type <= 1) {
                            value = Float.parseFloat(sValue);
                            if (value <= 0.0f) {
                                throw new NumberFormatException();
                            }
                        }

                        double newDPI;
                        if (AnonymousClass2.this.val$type == 0) {
                            newDPI = SpiritLevelActivity2.getDPI(AnonymousClass2.this.val$context.getResources().getDisplayMetrics().widthPixels, AnonymousClass2.this.val$context.getResources().getDisplayMetrics().heightPixels, (double) value);
                        } else {
                            newDPI = (double) value;
                        }
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(AnonymousClass2.this.val$context).edit();
                        editor.putFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, (float) newDPI);
                        editor.apply();
                        AnonymousClass2.this.val$calibrationPref.setSummary(String.format(Locale.US, "%s: %.2f", new Object[]{AnonymousClass2.this.val$context.getResources().getString(R.string.IDS_CALIBRATION_CURRENT_PPI), Double.valueOf(newDPI)}));
                        AnonymousClass2.this.val$d.dismiss();
                    } catch (NumberFormatException e) {
                        Toast.makeText(AnonymousClass2.this.val$context, AnonymousClass2.this.val$context.getResources().getString(R.string.IDS_CALIBRATION_INVALID_VALUE), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public static class SettingsKey {
        public static final String SETTINGS_ALTIMETER_GET_METHOD_KEY = "SETTINGS_ALTIMETER_GET_METHOD_KEY";
        public static final String SETTINGS_ALTIMETER_UNIT_KEY = "SETTINGS_ALTIMETER_UNIT_KEY";
        public static final String SETTINGS_COMPASS_HEADING_KEY = "SETTINGS_COMPASS_HEADING_KEY";
        public static final String SETTINGS_DECIBEL_CALIBRATION_VALUE_KEY = "SETTINGS_DECIBEL_CALIBRATION_VALUE_KEY";
        public static final String SETTINGS_DECIBEL_UPDATE_FREQUENCY_KEY = "SETTINGS_DECIBEL_UPDATE_FREQUENCY_KEY";
        public static final String SETTINGS_GENERAL_LAST_USED_TOOL_KEY = "SETTINGS_GENERAL_LAST_USED_TOOL_KEY";
        public static final String SETTINGS_GENERAL_MENU_ORIENTATION_KEY = "SETTINGS_GENERAL_MENU_ORIENTATION_KEY";
        public static final String SETTINGS_GENERAL_SHOW_TOOLTIP_KEY = "SETTINGS_GENERAL_SHOW_TOOLTIP_KEY";
        public static final String SETTINGS_GENERAL_SOUNDFX_KEY = "SETTINGS_GENERAL_SOUNDFX_KEY";
        public static final String SETTINGS_IS_ALTIMETER_UNLOCKED = "SETTINGS_IS_ALTIMETER_UNLOCKED";
        public static final String SETTINGS_IS_BAROMETER_UNLOCKED = "SETTINGS_IS_BAROMETER_UNLOCKED";
        public static final String SETTINGS_IS_PREMIUM = "SETTINGS_IS_PREMIUM";
        public static final String SETTINGS_METRONOME_BPM_KEY = "SETTINGS_METRONOME_BPM_KEY";
        public static final String SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY = "SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY";
        public static final String SETTINGS_METRONOME_FLASH_ENABLED_KEY = "SETTINGS_METRONOME_FLASH_ENABLED_KEY";
        public static final String SETTINGS_METRONOME_FLASH_TYPE_KEY = "SETTINGS_METRONOME_FLASH_TYPE_KEY";
        public static final String SETTINGS_METRONOME_LAST_WHEEL_ANGLE_KEY = "SETTINGS_METRONOME_LAST_WHEEL_ANGLE_KEY";
        public static final String SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY = "SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY";
        public static final String SETTINGS_METRONOME_METER_KEY = "SETTINGS_METRONOME_METER_KEY";
        public static final String SETTINGS_METRONOME_SUB_BEAT_KEY = "SETTINGS_METRONOME_SUB_BEAT_KEY";
        public static final String SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY = "SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY";
        public static final String SETTINGS_METRONOME_SUB_DIVISION_KEY = "SETTINGS_METRONOME_SUB_DIVISION_KEY";
        public static final String SETTINGS_METRONOME_VOLUME_KEY = "SETTINGS_METRONOME_VOLUME_KEY";
        public static final String SETTINGS_PLUMB_BOB_CALIB_ANGLE_OXY = "SETTINGS_PLUMB_BOB_CALIB_ANGLE_OXY";
        public static final String SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZX = "SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZX";
        public static final String SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZY = "SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZY";
        public static final String SETTINGS_PLUMB_BOB_CALIB_MATRIX_KEY = "SETTINGS_PLUMB_BOB_CALIB_MATRIX_%d%d_KEY";
        public static final String SETTINGS_PLUMB_BOB_LOCK_KEY = "SETTINGS_PLUMB_BOB_LOCK_KEY";
        public static final String SETTINGS_PLUMB_BOB_RESET_CALIBRATION = "SETTINGS_PLUMB_BOB_RESET_CALIBRATION";
        public static final String SETTINGS_PLUMB_BOB_SENSITIVITY_KEY = "SETTINGS_PLUMB_BOB_SENSITIVITY_KEY";
        public static final String SETTINGS_PLUMB_BOB_UNIT_KEY = "SETTINGS_PLUMB_BOB_UNIT_KEY";
        public static final String SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY = "SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY = "SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_LAST_ANGLE_KEY = "SETTINGS_PROTRACTOR_LAST_ANGLE_KEY";
        public static final String SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY = "SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY";
        public static final String SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY = "SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY = "SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY = "SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY";
        public static final String SETTINGS_PROTRACTOR_SHOW_HAND_LINE_KEY = "SETTINGS_PROTRACTOR_SHOW_HAND_LINE_KEY";
        public static final String SETTINGS_PROTRACTOR_SNAPPING_KEY = "SETTINGS_PROTRACTOR_SNAPPING_KEY";
        public static final String SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY = "SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY";
        public static final String SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY = "SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY";
        public static final String SETTINGS_RULER_CENTIMETER_SNAP_KEY = "SETTINGS_RULER_CENTIMETER_SNAP_KEY";
        public static final String SETTINGS_RULER_CURRENT_PAGE_KEY = "SETTINGS_RULER_CURRENT_PAGE_KEY";
        public static final String SETTINGS_RULER_CURRENT_X_KEY = "SETTINGS_RULER_CURRENT_X_KEY";
        public static final String SETTINGS_RULER_DPI_KEY = "SETTINGS_RULER_DPI_KEY";
        public static final String SETTINGS_RULER_HFLIPPED_KEY = "SETTINGS_RULER_HFLIPPED_KEY";
        public static final String SETTINGS_RULER_INCH_SNAP_KEY = "SETTINGS_RULER_INCH_SNAP_KEY";
        public static final String SETTINGS_RULER_PICA_SNAP_KEY = "SETTINGS_RULER_PICA_SNAP_KEY";
        public static final String SETTINGS_RULER_PIXEL_SNAP_KEY = "SETTINGS_RULER_PIXEL_SNAP_KEY";
        public static final String SETTINGS_RULER_PRIMARY_UNIT_KEY = "SETTINGS_RULER_PRIMARY_UNIT_KEY";
        public static final String SETTINGS_RULER_SECONDARY_UNIT_KEY = "SETTINGS_RULER_SECONDARY_UNIT_KEY";
        public static final String SETTINGS_RULER_SHOW_HAND_LINE_KEY = "SETTINGS_RULER_SHOW_HAND_LINE_KEY";
        public static final String SETTINGS_RULER_SNAPPING_KEY = "SETTINGS_RULER_SNAPPING_KEY";
        public static final String SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY = "SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY";
        public static final String SETTINGS_RULER_UNIT_TO_SNAP_KEY = "SETTINGS_RULER_UNIT_TO_SNAP_KEY";
        public static final String SETTINGS_SEISMOMETER_ALARM_ENABLED_KEY = "SETTINGS_SEISMOMETER_ALARM_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY = "SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY";
        public static final String SETTINGS_SEISMOMETER_ALARM_SOUND_KEY = "SETTINGS_SEISMOMETER_ALARM_SOUND_KEY";
        public static final String SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY = "SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY = "SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY = "SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_SCALE_TYPE_KEY = "SETTINGS_SEISMOMETER_SCALE_TYPE_KEY";
        public static final String SETTINGS_SEISMOMETER_TIMELINE_ENABLED_KEY = "SETTINGS_SEISMOMETER_TIMELINE_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_UPDATE_FREQUENCY_KEY = "SETTINGS_SEISMOMETER_UPDATE_FREQUENCY_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_CALIB_KEY_TEMPLATE = "SETTINGS_SPIRIT_LEVEL_CALIB_%d_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_LOCK_KEY = "SETTINGS_SPIRIT_LEVEL_LOCK_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_RESET_CALIBRATION_KEY = "SETTINGS_SPIRIT_LEVEL_RESET_CALIBRATION_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_SENSITIVITY_KEY = "SETTINGS_SPIRIT_LEVEL_SENSITIVITY_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_THEME_KEY = "SETTINGS_SPIRIT_LEVEL_THEME_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_UNIT_KEY = "SETTINGS_SPIRIT_LEVEL_UNIT_KEY";
        public static final String SETTINGS_STOPWATCH_RETAIN_ENABLED_KEY = "SETTINGS_STOPWATCH_RETAIN_ENABLED_KEY";
        public static final String SETTINGS_STOPWATCH_SPLIT_MODE_KEY = "SETTINGS_STOPWATCH_SPLIT_MODE_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_LEFT_X = "SETTINGS_SURFACE_LEVEL_CALIB_LEFT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_LEFT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_LEFT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_X = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Z = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Z";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Z = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Z";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_X = "SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_LOCK_KEY = "SETTINGS_SURFACE_LEVEL_LOCK_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_RESET_CALIBRATION_KEY = "SETTINGS_SURFACE_LEVEL_RESET_CALIBRATION_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_SENSITIVITY_KEY = "SETTINGS_SURFACE_LEVEL_SENSITIVITY_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_THEME_KEY = "SETTINGS_SURFACE_LEVEL_THEME_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_UNIT_KEY = "SETTINGS_SURFACE_LEVEL_UNIT_KEY";
        public static final String SETTINGS_TESLAMETER_ALERT_ENABLED_KEY = "SETTINGS_TESLAMETER_ALERT_ENABLED_KEY";
        public static final String SETTINGS_TESLAMETER_ALERT_SOUND_KEY = "SETTINGS_TESLAMETER_ALERT_SOUND_KEY";
        public static final String SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY = "SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY";
        public static final String SETTINGS_TESLAMETER_RUNNING_KEY = "SETTINGS_TESLAMETER_RUNNING_KEY";
        public static final String SETTINGS_TESLAMETER_SENSITIVITY_KEY = "SETTINGS_TESLAMETER_SENSITIVITY_KEY";
        public static final String SETTINGS_TESLAMETER_UNIT_KEY = "SETTINGS_TESLAMETER_UNIT_KEY";
        public static final String SETTINGS_TIMER_ALARM_SOUND_KEY_TEMPLATE = "SETTINGS_TIMER_ALARM_SOUND_%d_KEY";
        public static final String SETTINGS_TIMER_ALARM_SOUND_LOOP_KEY_TEMPLATE = "SETTINGS_TIMER_ALARM_SOUND_LOOP_%d_KEY";
        public static final String SETTINGS_TIMER_CONFIGURE_KEY_TEMPLATE = "SETTINGS_TIMER_CONFIGURE_%d_KEY";
        public static final String SETTINGS_TIMER_COUNT_DOWN_MODE_KEY = "SETTINGS_TIMER_COUNT_DOWN_MODE_KEY";
        public static final String SETTINGS_TIMER_DEFAULT_DURATION_KEY_TEMPLATE = "SETTINGS_TIMER_DEFAULT_DURATION_%d_KEY";
        public static final String SETTINGS_TIMER_MESSAGE_KEY_TEMPLATE = "SETTINGS_TIMER_MESSAGE_%d_KEY";
        public static final String SETTINGS_TIMER_NAME_KEY_TEMPLATE = "SETTINGS_TIMER_NAME_%d_KEY";
        public static final String SETTINGS_TIMER_REPEAT_ENABLED_KEY_TEMPLATE = "SETTINGS_TIMER_REPEAT_ENABLED_%d_KEY";
        public static final String SETTINGS_TIMER_RETAIN_ENABLED_KEY = "SETTINGS_TIMER_RETAIN_ENABLED_KEY";
    }

    /* renamed from: com.vivekwarde.measure.settings.SettingsActivity.1 */
    class AnonymousClass1 implements OnPreferenceClickListener {
        final /* synthetic */ Preference val$calibrationPref;

        AnonymousClass1(Preference preference) {
            this.val$calibrationPref = preference;
        }

        public boolean onPreferenceClick(Preference preference) {
            CharSequence[] str = new CharSequence[]{SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_1), SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_2), SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_3)};
            Builder b = new Builder(SettingsActivity.this);
            b.setTitle(SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION));
            b.setItems(str, new OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    SettingsActivity.onCalibrate(SettingsActivity.this, position, AnonymousClass1.this.val$calibrationPref);
                }
            });
            b.create().show();
            return false;
        }
    }
}

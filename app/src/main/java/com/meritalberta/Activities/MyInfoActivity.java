package com.meritalberta.Activities;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.meritalberta.R;

/**
 * Created by Bharat on 13-Dec-16.
 */

public class MyInfoActivity extends AppCompatActivity implements View.OnClickListener {

    public ImageView img_check;
    public EditText et_company_name, et_first_name, et_last_name, et_email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.my_info_activity);
        initView();
    }

    private void initView() {
        img_check = (ImageView) findViewById(R.id.img_check);
        img_check.setOnClickListener(this);
        et_company_name = (EditText) findViewById(R.id.et_company_name);
        et_company_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        findViewById(R.id.img_c_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_check:
                img_check.setSelected(!img_check.isSelected());
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }
}

package com.meritalberta.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meritalberta.Activities.MainActivity;
import com.meritalberta.AppDelegate;
import com.meritalberta.R;

/**
 * Created by Bharat on 13-Dec-16.
 */

public class ContactUsFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contact_us_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_CONTACT_US);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_CONTACT_US);
    }

    private void initView(View view) {
        view.findViewById(R.id.rl_edmon).setOnClickListener(this);
        view.findViewById(R.id.rl_calgary).setOnClickListener(this);

        view.findViewById(R.id.ll_c_call_1).setOnClickListener(this);
        view.findViewById(R.id.ll_c_call_2).setOnClickListener(this);

        view.findViewById(R.id.ll_c_email_1).setOnClickListener(this);
        view.findViewById(R.id.ll_c_email_2).setOnClickListener(this);
        view.findViewById(R.id.img_c_menu).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_c_call_1:
                AppDelegate.callNumber(getActivity(), getString(R.string.contact_01));
                break;
            case R.id.ll_c_email_1:
                AppDelegate.sendEmail(getActivity(), getString(R.string.email_01), "", "");
                break;
            case R.id.ll_c_call_2:
                AppDelegate.callNumber(getActivity(), getString(R.string.contact_02));
                break;
            case R.id.ll_c_email_2:
                AppDelegate.sendEmail(getActivity(), getString(R.string.email_02), "", "");
                break;
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }
}

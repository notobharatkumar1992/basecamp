package com.meritalberta.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meritalberta.Activities.MainActivity;
import com.meritalberta.Activities.WebViewDetailActivity;
import com.meritalberta.AppDelegate;
import com.meritalberta.Async.PostAsync;
import com.meritalberta.Models.NewsModel;
import com.meritalberta.Models.PostAysnc_Model;
import com.meritalberta.R;
import com.meritalberta.adapters.PagerAdapter;
import com.meritalberta.constants.ServerRequestConstants;
import com.meritalberta.constants.Tags;
import com.meritalberta.interfaces.OnReciveServerResponse;
import com.meritalberta.parser.JSONParser;
import com.meritalberta.spirit_level.SpiritLevelActivity2;
import com.meritalberta.tetris.MyTetrisMain;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import carbon.widget.TextView;

/**
 * Created by Heena on 13-Dec-16.
 */

public class HomeFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {
    TextView txt_c_address_title1, txt_c_address_title2, txt_c_address_subtitle2, txt_c_address_subtitle1;
    //    ImageView img_c_member_service, img_c_news, img_c_perks, img_c_college, img_c_benefits, img_c_merit_blocks_game;
    ViewPager viewpager_news;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();
    ArrayList<NewsModel> newsModelArrayList = new ArrayList<>();

    public Handler mHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setHandler();
        mHandler.sendEmptyMessage(4);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_HOME);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_HOME);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 1:
                        setBanner();
                        break;
                    case 2:
                        break;
                    case 4:
                        execute_newsList();
                        break;
                }
            }
        };
    }

    private void execute_newsList() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            try {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                //  AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.NEWS_LIST,mPostArrayList, this);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void initView(View view) {
        view.findViewById(R.id.rl_level).setOnClickListener(this);
        view.findViewById(R.id.img_c_menu).setOnClickListener(this);
        view.findViewById(R.id.img_c_call).setOnClickListener(this);
        view.findViewById(R.id.img_c_email).setOnClickListener(this);
        txt_c_address_title1 = (TextView) view.findViewById(R.id.txt_c_address_title1);
        txt_c_address_title2 = (TextView) view.findViewById(R.id.txt_c_address_title2);
        txt_c_address_subtitle1 = (TextView) view.findViewById(R.id.txt_c_address_subtitle1);
        txt_c_address_subtitle2 = (TextView) view.findViewById(R.id.txt_c_address_subtitle2);

        view.findViewById(R.id.rl_c_previous).setOnClickListener(this);
        view.findViewById(R.id.rl_c_next).setOnClickListener(this);
        viewpager_news = (ViewPager) view.findViewById(R.id.viewpager_news);

        view.findViewById(R.id.ll_c_member_service).setOnClickListener(this);
        view.findViewById(R.id.ll_c_news).setOnClickListener(this);
        view.findViewById(R.id.ll_c_perks).setOnClickListener(this);
        view.findViewById(R.id.ll_c_college).setOnClickListener(this);
        view.findViewById(R.id.ll_c_benefits).setOnClickListener(this);
        view.findViewById(R.id.ll_c_merit_blocks_game).setOnClickListener(this);
        setBanner();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_level:
                startActivity(new Intent(getActivity(), SpiritLevelActivity2.class));
                break;
            case R.id.img_c_call:
                AppDelegate.callNumber(getActivity(), "184845545");
                break;
            case R.id.img_c_email:
                AppDelegate.sendEmail(getActivity(), "mail_id", "subject", "extra text");
                break;
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
            case R.id.rl_c_previous:
                if (viewpager_news.getAdapter().getCount() > 0)
                    if (viewpager_news.getCurrentItem() == 0) {
                        viewpager_news.setCurrentItem(viewpager_news.getAdapter().getCount() - 1);
                    } else {
                        viewpager_news.setCurrentItem(viewpager_news.getCurrentItem() - 1);
                    }
                break;
            case R.id.rl_c_next:
                if (viewpager_news.getAdapter().getCount() > 0)
                    if (viewpager_news.getCurrentItem() == viewpager_news.getAdapter().getCount() - 1) {
                        viewpager_news.setCurrentItem(0);
                    } else {
                        viewpager_news.setCurrentItem(viewpager_news.getCurrentItem() + 1);
                    }
                break;
            case R.id.ll_c_member_service:
                Intent intent = new Intent(getActivity(), WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.MEMBER_SERVICE);
                intent.putExtra(Tags.title, Tags.member_services);
                startActivity(intent);
                break;
            case R.id.ll_c_news:
                intent = new Intent(getActivity(), WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.NEWS);
                intent.putExtra(Tags.title, Tags.merit_tv);
                startActivity(intent);
                break;
            case R.id.ll_c_perks:
                intent = new Intent(getActivity(), WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.PERKS);
                intent.putExtra(Tags.title, Tags.perks);
                startActivity(intent);
                break;
            case R.id.ll_c_benefits:
                intent = new Intent(getActivity(), WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.BENEFITS);
                intent.putExtra(Tags.title, Tags.benefits);
                startActivity(intent);
                break;
            case R.id.ll_c_college:
                intent = new Intent(getActivity(), WebViewDetailActivity.class);
                intent.putExtra(Tags.website_url, ServerRequestConstants.TRAINING);
                intent.putExtra(Tags.title, Tags.training);
                startActivity(intent);
                break;
            case R.id.ll_c_merit_blocks_game:
                startActivity(new Intent(getActivity(), MyTetrisMain.class));
                break;
        }
    }

    void setBanner() {
        if (newsModelArrayList != null) {
            for (int i = 0; i < newsModelArrayList.size(); i++) {
                Fragment fragment = new BannerFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.DATA, newsModelArrayList.get(i));
                fragment.setArguments(bundle);
                bannerFragment.add(fragment);
            }
            AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
            bannerPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), bannerFragment);
            viewpager_news.setAdapter(bannerPagerAdapter);
            setUiPageViewController();
            viewpager_news.setCurrentItem(0);
        }

    }

    private void setUiPageViewController() {
        viewpager_news.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(getActivity(), getString(R.string.time_out));
            return;
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.NEWS_LIST)) {
            parseNewsResponse(result);
        }
    }

    private void parseNewsResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.DataFlow);
                newsModelArrayList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    NewsModel newsModel = new NewsModel();
                    newsModel.image = JSONParser.getString(object, Tags.image);
                    newsModel.title = JSONParser.getString(object, Tags.title);
                    newsModel.content = JSONParser.getString(object, Tags.content);
                    newsModel.link = JSONParser.getString(object, Tags.link);
                    if (AppDelegate.isValidString(newsModel.link))
                        newsModelArrayList.add(newsModel);
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(getActivity(), getResources().getString(R.string.alert), JSONParser.getString(jsonObject, Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Server under maintenance, please try after some time.");
        }
    }

}
